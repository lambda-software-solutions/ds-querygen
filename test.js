require('babel-polyfill');
const Serializer = require('./src/Serializer');
const registerSerializers = require('./src/serializers');
const xform = require('./src/transforms');
const Schema = require('./lib/Schema');

const schema = Schema.from({
  version: '1.0',
  refs: {
    created_at: {
      title: 'Created At',
      column: 'created_at',
      type: 'timestamp'
    },
    updated_at: {
      title: 'Updated At',
      column: 'updated_at',
      type: 'timestamp'
    },
    region: {
      title: 'Region',
      column: 'region',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Timezone',
        column: 'timezone',
        type: 'string'
      }]
    },
    category: {
      title: 'Category',
      column: 'category',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }]
    },
    customer: {
      title: 'Customer',
      column: 'customer',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }]
    },
    department: {
      title: 'Department',
      column: 'department',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Customer ID',
        column: 'customerId',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }]
    },
    location: {
      title: 'Location',
      column: 'location',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Market Code',
        column: 'marketCode',
        type: 'string'
      }, {
        title: 'City',
        column: 'city',
        type: 'string'
      }, {
        title: 'Address',
        column: 'streetAddress',
        type: 'string'
      }, {
        title: 'Address Line 2',
        column: 'streetAddress2',
        type: 'string'
      }, {
        title: 'State',
        column: 'state',
        type: 'string'
      }, {
        title: 'Postal Code',
        column: 'postalCode',
        type: 'string'
      }, {
        title: 'Phone',
        column: 'phone',
        type: 'string'
      }, {
        title: 'Fax',
        column: 'fax',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Latitude',
        column: 'lat',
        type: 'float'
      }, {
        title: 'Longitude',
        column: 'lon',
        type: 'float'
      }, {
        title: 'Location Type ID',
        column: 'locationTypeId',
        type: 'uuid'
      }, {
        title: 'Region ID',
        column: 'regionId',
        type: 'uuid'
      }]
    },
    provider: {
      title: 'Provider',
      column: 'provider',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Type',
        column: 'providerType',
        type: 'string'
      }, {
        title: 'First Name',
        column: 'firstName',
        type: 'string'
      }, {
        title: 'Last Name',
        column: 'lastName',
        type: 'string'
      }]
    },
    service: [{
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      title: 'Name',
      column: 'name',
      type: 'string'
    }, {
      title: 'Rate',
      column: 'rate',
      type: 'float'
    }, {
      title: 'Overtime Rate',
      column: 'overtimeRate',
      type: 'float'
    }, {
      title: 'Transportation Surcharge',
      column: 'transportationSurcharge',
      type: 'float'
    }, {
      title: 'Surcharge ID',
      column: 'surchargeId',
      type: 'uuid'
    }, {
      title: 'Category ID',
      column: 'categoryId',
      type: 'uuid'
    }, {
      title: 'Customer ID',
      column: 'customerId',
      type: 'uuid'
    }, {
      title: 'Region ID',
      column: 'regionId',
      type: 'uuid'
    }]
  },
  collections: [{
    title: 'Users',
    table: 'user',
    fields: [{
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      title: 'Profile',
      column: 'profile',
      type: 'struct',
      fields: [{
        title: 'First Name',
        column: 'firstName',
        type: 'string'
      }, {
        title: 'Last Name',
        column: 'lastName',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }]
    }, {
      title: 'Login Count',
      column: 'login_count',
      type: 'int'
    }, {
      title: 'Enabled?',
      column: 'is_enabled',
      type: 'bool'
    }, {
      title: 'Created At',
      column: 'created_at',
      type: 'timestamp'
    }, {
      title: 'Updated At',
      column: 'updated_at',
      type: 'timestamp'
    }],
    relationships: [{
      type: 'hasMany',
      collection: 'order'
    }]
  }, {
    title: 'Orders',
    table: 'order',
    fields: [{
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      title: 'Type',
      column: 'type',
      type: 'string'
    }, {
      title: 'PL Number',
      column: 'pl_number',
      type: 'int'
    }, {
      title: 'Region',
      column: 'region',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Timezone',
        column: 'timezone',
        type: 'string'
      }]
    }, {
      title: 'Department',
      column: 'department',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Customer ID',
        column: 'customerId',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }]
    }, {
      column: 'billing_location',
      title: 'Location',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Market Code',
        column: 'marketCode',
        type: 'string'
      }, {
        title: 'City',
        column: 'city',
        type: 'string'
      }, {
        title: 'Address',
        column: 'streetAddress',
        type: 'string'
      }, {
        title: 'Address Line 2',
        column: 'streetAddress2',
        type: 'string'
      }, {
        title: 'State',
        column: 'state',
        type: 'string'
      }, {
        title: 'Postal Code',
        column: 'postalCode',
        type: 'string'
      }, {
        title: 'Phone',
        column: 'phone',
        type: 'string'
      }, {
        title: 'Fax',
        column: 'fax',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Latitude',
        column: 'lat',
        type: 'float'
      }, {
        title: 'Longitude',
        column: 'lon',
        type: 'float'
      }, {
        title: 'Location Type ID',
        column: 'locationTypeId',
        type: 'uuid'
      }, {
        title: 'Region ID',
        column: 'regionId',
        type: 'uuid'
      }]
    }, {
      title: 'Customer Work Order',
      column: 'customer_work_order',
      type: 'string'
    }, {
      title: 'Windfall Work Order',
      column: 'windfall_work_order',
      type: 'string'
    }, {
      title: 'Start Date',
      column: 'start_date',
      type: 'date'
    }, {
      title: 'End Date',
      column: 'end_date',
      type: 'date'
    }, {
      title: 'Requester',
      column: 'requester',
      type: 'string'
    }, {
      title: 'Description',
      column: 'description',
      type: 'string'
    }, {
      title: 'Notes (internal)',
      column: 'notes',
      type: 'string'
    }, {
      title: 'Repair?',
      column: 'is_repair',
      type: 'bool'
    }, {
      title: 'Overtime Authorized?',
      column: 'is_overtime_authorized',
      type: 'bool'
    }, {
      title: 'Weight',
      column: 'weight',
      type: {
        type: 'numeric',
        prevision: 8,
        scale: 4
      }
    }, {
      title: 'Created At',
      column: 'created_at',
      type: 'timestamp'
    }, {
      title: 'Updated At',
      column: 'updated_at',
      type: 'timestamp'
    }],
    relationships: [{
      type: 'belongsTo',
      collection: 'user'
    }, {
      type: 'hasMany',
      collection: 'service'
    }]
  }, {
    title: 'Service Items',
    table: 'service',
    fields: [{
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      title: 'Order ID',
      column: 'order_id',
      type: 'uuid'
    }, {
      title: 'Region',
      column: 'region',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Timezone',
        column: 'timezone',
        type: 'string'
      }]
    }, {
      title: 'Provider',
      column: 'provider',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Type',
        column: 'providerType',
        type: 'string'
      }, {
        title: 'First Name',
        column: 'firstName',
        type: 'string'
      }, {
        title: 'Last Name',
        column: 'lastName',
        type: 'string'
      }]
    }, {
      column: 'origin',
      title: 'Location',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Market Code',
        column: 'marketCode',
        type: 'string'
      }, {
        title: 'City',
        column: 'city',
        type: 'string'
      }, {
        title: 'Address',
        column: 'streetAddress',
        type: 'string'
      }, {
        title: 'Address Line 2',
        column: 'streetAddress2',
        type: 'string'
      }, {
        title: 'State',
        column: 'state',
        type: 'string'
      }, {
        title: 'Postal Code',
        column: 'postalCode',
        type: 'string'
      }, {
        title: 'Phone',
        column: 'phone',
        type: 'string'
      }, {
        title: 'Fax',
        column: 'fax',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Latitude',
        column: 'lat',
        type: 'float'
      }, {
        title: 'Longitude',
        column: 'lon',
        type: 'float'
      }, {
        title: 'Location Type ID',
        column: 'locationTypeId',
        type: 'uuid'
      }, {
        title: 'Region ID',
        column: 'regionId',
        type: 'uuid'
      }]
    }, {
      column: 'destination',
      title: 'Location',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }, {
        title: 'Market Code',
        column: 'marketCode',
        type: 'string'
      }, {
        title: 'City',
        column: 'city',
        type: 'string'
      }, {
        title: 'Address',
        column: 'streetAddress',
        type: 'string'
      }, {
        title: 'Address Line 2',
        column: 'streetAddress2',
        type: 'string'
      }, {
        title: 'State',
        column: 'state',
        type: 'string'
      }, {
        title: 'Postal Code',
        column: 'postalCode',
        type: 'string'
      }, {
        title: 'Phone',
        column: 'phone',
        type: 'string'
      }, {
        title: 'Fax',
        column: 'fax',
        type: 'string'
      }, {
        title: 'Email',
        column: 'email',
        type: 'string'
      }, {
        title: 'Latitude',
        column: 'lat',
        type: 'float'
      }, {
        title: 'Longitude',
        column: 'lon',
        type: 'float'
      }, {
        title: 'Location Type ID',
        column: 'locationTypeId',
        type: 'uuid'
      }, {
        title: 'Region ID',
        column: 'regionId',
        type: 'uuid'
      }]
    }, {
      title: 'Completed At',
      column: 'completed_date',
      type: 'timestamp'
    }, {
      title: 'Scheduled At',
      column: 'scheduled_date',
      type: 'timestamp'
    }, {
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      column: 'service_category',
      title: 'Category',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Name',
        column: 'name',
        type: 'string'
      }]
    }, {
      title: 'Line Number',
      column: 'line_number',
      type: 'int'
    }, {
      title: 'Description',
      column: 'description',
      type: 'text'
    }, {
      title: 'Time Estimate',
      column: 'time_estimate',
      type: {
        type: 'numeric',
        prevision: 8,
        scale: 4
      }
    }, {
      title: 'All Day?',
      column: 'all_day',
      type: 'bool'
    }, {
      title: 'Has Documents?',
      column: 'has_documents',
      type: 'bool'
    }, {
      title: 'Created At',
      column: 'created_at',
      type: 'timestamp'
    }, {
      title: 'Updated At',
      column: 'updated_at',
      type: 'timestamp'
    }],
    relationships: [{
      type: 'belongsTo',
      collection: 'order'
    }, {
      type: 'hasMany',
      collection: 'billing'
    }]
  }, {
    title: 'Billings',
    table: 'billing',
    fields: [{
      title: 'ID',
      column: 'id',
      type: 'uuid'
    }, {
      title: 'Service ID',
      column: 'service_id',
      type: 'uuid'
    }, {
      title: 'Rate',
      column: 'rate',
      type: {
        type: 'numeric',
        prevision: 12,
        scale: 2
      }
    }, {
      title: 'Time',
      column: 'time',
      type: 'real'
    }, {
      title: 'Provider',
      column: 'provider',
      type: 'struct',
      fields: [{
        title: 'ID',
        column: 'id',
        type: 'uuid'
      }, {
        title: 'Type',
        column: 'providerType',
        type: 'string'
      }, {
        title: 'First Name',
        column: 'firstName',
        type: 'string'
      }, {
        title: 'Last Name',
        column: 'lastName',
        type: 'string'
      }]
    }, {
      title: 'Activity Date',
      column: 'activity_date',
      type: 'date'
    }, {
      title: 'Exception Explanation',
      column: 'exception_explanation',
      type: 'text'
    }, {
      title: 'Has Exception?',
      column: 'has_exception',
      type: 'bool'
    }, {
      title: 'Is Exception\xA0Approved?',
      column: 'is_exception_approved',
      type: 'bool'
    }, {
      title: 'Created At',
      column: 'created_at',
      type: 'timestamp'
    }, {
      title: 'Updated At',
      column: 'updated_at',
      type: 'timestamp'
    }],
    relationships: [{
      type: 'belongsTo',
      collection: 'service'
    }]
  }]
});

const serializer = new Serializer();
registerSerializers(serializer);

let query = {
  filter: {
    type: "and",
    args: [
      {
        fn: 'inThisWeek',
        args: [
          {
            type: "column",
            path: "service.scheduled_date"
          }
        ]
      },
      {
        type: "startsWith",
        lh: {
          type: "column",
          path: "order.requester"
        },
        rh: "j",
        i: true
      }
    ]
  },
  collection: "order",
  columns: [
    {
      path: "order.pl_number",
      type: "column"
    }
  ]
};
query = xform(schema, query);

console.log('query', JSON.stringify(query, null, 3))
console.log(serializer.serialize(query));