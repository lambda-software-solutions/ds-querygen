const { groupBy } = require('./util');

const STRING_TYPES = new Set([ 'string', 'text', 'uuid' ]);
const CARDINALITY = Object.freeze({
    ONE: 'ONE',
    MANY: 'MANY',
    UNDEFINED: 'UNDEFINED'
});

class CollectionLike {

    fieldForColumn(column) {
        if (!this._fieldsByColumn) {
            this._indexFieldsByColumn();
        }

        return this._fieldsByColumn[column];
    }

    _indexFieldsByColumn() {
        this._fieldsByColumn = this.fields.reduce((byColumn, field) => {
            const fieldColumn = field.column;
            return Object.assign(byColumn, { [fieldColumn]: field });
        }, {});
    }
}

class Field extends CollectionLike {
    static from(obj) {
        const { column, type, name, fields = [] } = obj;
        return new Field(column, type, name, fields);
    }

    constructor(column, type, name, fields) {
        super();
        this.column = column;
        this.type = type;
        this.name = name;
        this.fields = fields ? fields.map(Field.from) : null;
    }

    get isStringLike() {
        return STRING_TYPES.has(this.type);
    }

    lookupEmbedded(property) {
        if (property.length === 0) {
            return this;
        }

        const [ nextSegment, ...restSegments ] = property;
        const nextField = this.fieldForColumn(nextSegment);

        if (!nextField) {
            return;
        }

        return nextField.lookupEmbedded(restSegments);
    }
}

class Relationship {

    constructor(left, right, cardinality, foreignKey = null) {
        if (typeof CARDINALITY[cardinality] === 'undefined') {
            throw new Error(`Invalid cardinality: ${cardinality}`);
        }

        this.left = left;
        this.right = right;
        this.foreignKey = foreignKey || `${left}_id`;
        this.cardinality = cardinality;
    }

    cardinalityIndependentHashCode() {
        return `${this.left}:${this.right}`;
    }

    get isCardinalityDefined() {
        return this.cardinality !== CARDINALITY.UNDEFINED;
    }

    get leftId() {
        return (this._foreignKeyOnRighthand) ? 'id' : this.foreignKey;
    }

    get rightId() {
        return (this._foreignKeyOnRighthand) ? this.foreignKey : 'id';
    }

    get _foreignKeyOnRighthand() {
        return this.cardinality !== CARDINALITY.ONE;
    }
}

class Collection extends CollectionLike {

    static from(obj) {
        const { table, title, fields = [], relationships = [] } = obj;
        return new Collection(table, title, fields, relationships);
    }

    constructor(table, title, fields, relationships) {
        super();
        this.table = table;
        this.title = title;
        this.fields = fields.map(Field.from);
        this.relationships = relationships;
    }
}


class Schema {

    static from(obj) {
        const { version, collections = [] } = obj;
        return new Schema(version, collections);
    }

    constructor(version, collections) {
        this.version = version;
        this.collections = collections.map(Collection.from);
        this.relationships = buildRelationships(this.collections);
    }

    collectionForTable(tableName) {
        const collection = this.collections.find(c => c.table === tableName);
        if (!collection) {
            throw new Error(`No collection in schema for table: ${tableName}`);
        }
        return collection;
    }

    get orderedCollections() {
        if (this.relationships.length === 0) {
            return [this.collections[0]];
        }
        const linkedCollsByLeft = this.relationships.reduce((acc, rel) => {
            const coll = this.collectionForTable(rel.right)
            if (acc[rel.left]) {
                acc[rel.left].push(coll);
            } else {
                acc[rel.left] = [coll];
            }

            return acc;
        }, {});

        // Topo sort via depth-first search
        // see https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
        let sorted = [];
        let marksTmp = new Set();
        let marksPerm = new Set();
        function visit(coll) {
            if (marksPerm.has(coll)) {
                return;
            }
            if (marksTmp.has(coll)) {
                throw new Error('Cycle in relationships!');
            }
            marksTmp.add(coll);
            const linkedColls = linkedCollsByLeft[coll.table];
            if (linkedColls) {
                linkedColls.forEach(visit);
            }
            marksTmp.delete(coll);
            marksPerm.add(coll);
            sorted.unshift(coll);
        }
        while (marksTmp.size + marksPerm.size < this.collections.length) {
            visit(this.collections.find(coll => !marksTmp.has(coll) && !marksPerm.has(coll)));
        }

        return sorted;
    }

    findPathUp(desc, ances) {
        const nextRels = this._relationshipsByRight[desc];
        if (!nextRels) {
            return null;
        }

        // Search all direct descendants before attempting to recurse.
        // This breadth-first search will ensure that the shortest path
        // will be chosen.
        for (let rel of nextRels) {
            const { left, right } = rel;
            if (right === desc && left === ances) {
                return [rel];
            }
        }

        for (let rel of nextRels) {
            let nextPath = this.findPathUp(rel.left, ances);
            if (nextPath) {
                return [rel, ...nextPath];
            }
        }

        return null;
    }

    findPathDown(ances, desc) {
        const nextRels = this._relationshipsByLeft[ances];
        if (!nextRels) {
            return null;
        }

        for (let rel of nextRels) {
            const { left, right } = rel;
            if (right === desc && left === ances) {
                return [rel];
            }
        }

        for (let rel of nextRels) {
            let nextPath = this.findPathDown(rel.right, desc);
            if (nextPath) {
                return [rel, ...nextPath];
            }
        }

        return null;
    }

    // TODO: Memoize these
    get _relationshipsByLeft() {
        return groupBy(this.relationships, rel => rel.left);
    }

    get _relationshipsByRight() {
        return groupBy(this.relationships, rel => rel.right);
    }

    static get CARDINALITY() {
        return CARDINALITY;
    }
}

function buildRelationships(collections) {
    const allRelationships = allCollectionRelationships(collections);
    const rels = deduplicateRels(allRelationships);
    assertAllCardinalityDefined(rels);

    return rels;
}

function allCollectionRelationships(collections) {
    return collections.flatMap(collection =>
        collection.relationships.map(rel =>
            relationshipForCollectionRel(collection, rel)));
}

function relationshipForCollectionRel(collection, relationship) {
    const { type, collection: rel } = relationship;

    switch (type) {
        case 'belongsTo':
            return new Relationship(rel, collection.table, CARDINALITY.UNDEFINED);
        case 'hasOne':
            return new Relationship(rel, collection.table, CARDINALITY.ONE, `${collection.table}_id`);
        case 'hasMany':
            return new Relationship(collection.table, rel, CARDINALITY.MANY);
    }

    throw new Error(`Invalid relationship type: ${type}`);
}

function deduplicateRels(allRelationships) {
    const rels = allRelationships.reduce((acc, rel) => {
        const hash = rel.cardinalityIndependentHashCode();
        const prevRel = acc.get(hash);
        if (prevRel && prevRel.isCardinalityDefined) {
            rel = prevRel;
        }

        return acc.set(hash, rel);
    }, new Map());

    return [...rels.values()];
}

function assertAllCardinalityDefined(rels) {
    const firstUndefined = rels.find(rel => !rel.isCardinalityDefined);
    if (firstUndefined) {
        const actualRelDescription = `${firstUndefined.left} belongsTo ${firstUndefined.right}`;
        const possibleSolutionDescriptions = [
            `${firstUndefined.right} hasOne ${firstUndefined.left}`,
            `${firstUndefined.right} hasMany ${firstUndefined.left}`
        ];
        const msg = `Every belongsTo relationship requires a corresponding hasOne or hasMany:\nFound:\n\t${actualRelDescription}\nPossible solutions:\n\t${possibleSolutionDescriptions.join('\n\t')}`;
        throw new Error(msg);
    }
}

module.exports = Schema;