import 'babel-polyfill';
import transforms from './transforms';
import Schema from './Schema';
import Serializer from './Serializer';
import registerSerializers from './serializers';

const defaultSerializer = new Serializer();
registerSerializers(defaultSerializer);

export default {
    transforms,
    Schema,
    Serializer,
    defaultSerializer
};