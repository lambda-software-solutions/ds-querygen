const { when, preorderReplace, or } = require('../util');
const { isNodeType } = require('./util');

function fieldToPath(node) {
    return {
        type: 'path',
        path: node.path.split('.'),
        quote: true
    }
}

function convertFieldToPath(_schema, query) {
    return preorderReplace(query, when(
        or(isNodeType('field'), isNodeType('column')), fieldToPath
    ));
}

module.exports = convertFieldToPath;