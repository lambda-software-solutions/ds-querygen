function addFrom(_schema, query) {
    query.from = {
        type: 'from',
        tables: [
            { type: 'path', path: [query._target], quote: true }
        ]
    }
    return query;
}

module.exports = addFrom;