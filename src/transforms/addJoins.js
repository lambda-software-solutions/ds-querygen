const { and, when, preorderTraverse, butLast } = require('../util');
const { isNodeType } = require('./util');

// TODO: Support adding joins to subqueries
function addJoins(schema, query) {
    let queryCollections = allQueryCollections(query);
    queryCollections.delete(query._target);
    if (queryCollections.size === 0) {
        return query;
    }
    queryCollections = [...queryCollections.values()];

    const { orderedCollections } = schema;
    // Create ordered lists of all collections below and above the target,
    // sorted from closest to farthest from target
    const collectionsDownFromTarget = [query._target];
    const collectionsUpFromTarget = [];
    const pivotIdx = orderedCollections.findIndex(coll => coll.table === query._target);
    queryCollections
        .map(qc => ({
            idx: orderedCollections.findIndex(oc => oc.table === qc),
            collection: qc
        }))
        .sort(({ idx: a }, { idx: b }) => a - b)
        .forEach(({ idx, collection }) => {
            if (pivotIdx < idx) {
                collectionsDownFromTarget.push(collection);
            } else {
                collectionsUpFromTarget.unshift(collection);
            }
        });
    collectionsUpFromTarget.unshift(query._target);

    query.joins = {
        type: 'joins',
        joins: [
            ...joinsBelowTarget(schema, collectionsDownFromTarget),
            ...joinsAboveTarget(schema, collectionsUpFromTarget)
        ]
    };

    return query;
}

function joinCollectionsHigherThanTarget(schema, collection, target) {
    const joinRels = schema.findPathUp(target, collection);
    if (!joinRels) {
        return null;
    }

    return joinRels.map(joinRel => ({
        type: 'join',
        joinType: 'INNER',
        table: { type: 'ident', val: joinRel.left, quote: true },
        expr: {
          type: 'eq',
            lh: {
                type: 'path',
                path: [joinRel.right, joinRel.rightId],
                quote: true
            },
            rh: {
                type: 'path',
                path: [joinRel.left, joinRel.leftId],
                quote: true
            }
        }
    }));
}

// TODO: Do we want different join semantics depending on whether the cardinality
// of the relationship is one or many?
function joinCollectionsLowerThanTarget(schema, collection, target) {
    const joinRels = schema.findPathDown(target, collection);
    if (!joinRels) {
        return null;
    }

    return joinRels.map(joinRel => ({
        type: 'join',
        joinType: 'LEFT OUTER',
        table: { type: 'ident', val: joinRel.right, quote: true },
        expr: {
            type: 'eq',
            lh: {
                type: 'path',
                path: [joinRel.left, joinRel.leftId],
                quote: true
            },
            rh: {
                type: 'path',
                path: [joinRel.right, joinRel.rightId],
                quote: true
            }
        }
    }));
}

function allQueryCollections(query) {
    let collections = new Set();
    preorderTraverse(query, when(
        isNodeType('ident'),
        node => collections.add(node.val),

        and(isNodeType('path'), node => node.path.length > 1),
        node => butLast(node.path).forEach(c => collections.add(c)),
    ), isNodeType('subquery'));

    return collections;
}

function joinCollectionsWith(schema, queryCollections, joinFn) {
    let joins = [];
    let targetIdx = 0;
    let collIdx = 1;
    while (collIdx < queryCollections.length) {
        let target = queryCollections[targetIdx];
        let nextColl = queryCollections[collIdx];
        const nextJoins = joinFn(schema, nextColl, target);
        if (nextJoins) {
            joins = [...joins, ...nextJoins];
            // Advance both pointers so that the next join attempt is made between the
            // righthand side of the current join and the next-lower collection involved
            // in the query
            collIdx++;
            targetIdx++;
        } else if (targetIdx === 0) {
            // Cannot find any join path from target to collection
            return joins;
        } else {
            // Backtrack target by 1 to try to find join path from next-higher collection
            targetIdx--;
        }
    }

    return joins;
}

function joinsBelowTarget(schema, queryCollections) {
    return joinCollectionsWith(schema, queryCollections, joinCollectionsLowerThanTarget);
}

function joinsAboveTarget(schema, queryCollections) {
    return joinCollectionsWith(schema, queryCollections, joinCollectionsHigherThanTarget);
}

module.exports = addJoins;