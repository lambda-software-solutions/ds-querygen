const { when, preorderReplace } = require('../util');
const { isNodeType } = require('./util');

function predToNode(node) {
    node.type = node.op;
    delete node.op;
    return node;
}

function convertPredToNode(_schema, query) {
    return preorderReplace(query, when(
        isNodeType('pred'), predToNode
    ));
}

module.exports = convertPredToNode;