function inputToQuery(_schema, input) {
    const query = { type: 'query' };
    if (input.collection) {
        query._target = input.collection;
    }

    query.select = {
        type: 'select',
        exprs: input.selection || input.columns || []
    };

    if (input.filter) {
        query.where = {
            type: 'where',
            expr: input.filter
        };
    }

    if (input.pagination) {
        const { page = 0, perPage = 100 } = input.pagination;
        query.limit = {
            type: 'limit',
            limit: perPage,
            offset: page * perPage
        };
    }

    if (input.order) {
        const orders = Array.isArray(input.order) ? input.order : [input.order];
        query.order = {
            type: 'orderby',
            orders: orders.map(parseInputOrder)
        }
    }

    return query;
}

function parseInputOrder(orderNode) {
    const { column, expression, direction = 'ASC' } = orderNode;
    const node = { type: 'order', dir: direction };

    if (expression) {
        node.expr = expression;
    } else if (typeof column === 'string') {
        const path = column.split('.');
        if (path.length === 1) {
            node.expr = { type: 'ident', val: column, quote: true };
        } else {
            node.expr = { type: 'path', path, quote: true };
        }
    } else if (column && (column.type === 'path' || column.type === 'ident')) {
        node.expr = column;
    } else {
        throw new Error(`Cannot parse column or expression from order: ${JSON.stringify(orderNode)}`);
    }

    return node;
}

module.exports = inputToQuery;