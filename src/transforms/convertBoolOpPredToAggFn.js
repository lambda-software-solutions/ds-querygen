const { or, when, preorderReplace } = require('../util');
const { isNodeType } = require('./util');

const isBooleanAggrOp = or(
    isNodeType('bool_and'),
    isNodeType('bool_or')
);

function convertBoolOpPredToAggFn(_schema, query) {
    return preorderReplace(query, when(
        isBooleanAggrOp, node => ({
            type: 'eq',
            lh: {
                type: 'aggFn',
                aggFn: node.type,
                expr: node.lh
            },
            rh: true
        })
    ));
}

module.exports = convertBoolOpPredToAggFn;