const { when, and, preorderReplace, preorderTraverse } = require('../util');
const { isNodeType } = require('./util');

const isSubquery = isNodeType('subquery');

let tmpLog = false;

function annotateColumnsWithCollections(schema, query) {
    const targetCollection = schema.collectionForTable(query._target);
    const isInnerSubquery = and(isSubquery, node => node !== query);
    if (tmpLog) {
        console.log('Target for subquery: ', targetCollection.table);
    }

    const replacer = when(
        isNodeType('ident'), node => ({
            type: 'path',
            path: [targetCollection.table, node.val],
            quote: !!node.quote,
            _collection: targetCollection,
        }),

        isNodeType('path'), node => {
            if (node.path.length === 1) {
                return Object.assign(node, {
                    path: [targetCollection.table, ...node.path],
                    _collection: targetCollection
                });
            }

            return Object.assign(node, {
                _collection: node.path[0]
            });
        }
    );

    query.select = preorderReplace(query.select, replacer, isInnerSubquery);
    if (query.where) {
        query.where = preorderReplace(query.where, replacer, isInnerSubquery);
    }
    if (query.order) {
        query.order = preorderReplace(query.order, replacer, isInnerSubquery);
    }
    if (query.limit) {
        query.limit = preorderReplace(query.limit, replacer, isInnerSubquery);
    }

    preorderTraverse(query,
        when(
            isInnerSubquery,
            subquery => annotateColumnsWithCollections(schema, subquery),
        ),
        isInnerSubquery
    )

    return query;
}

module.exports = annotateColumnsWithCollections;