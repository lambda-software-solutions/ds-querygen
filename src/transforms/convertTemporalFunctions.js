const { when, preorderReplace } = require('../util');
const { isFnType } = require('./util');

const datePattern = /^\d{4}-\d{2}-\d{2}$/;
const timestampPattern = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}(?:Z|\+\d{2}:\d{2})$/;

const intervalMap = {
    day: 'day',
    days: 'day',
    hour: 'hour',
    hours: 'hour',
    minute: 'minute',
    minutes: 'minutes',
    second: 'second',
    second: 'second'
};

function strToTemporalNode(str) {
    if (datePattern.test(str)) {
        return {
            type: 'date',
            date: str
        };
    }
    if (timestampPattern.test(str)) {
        return {
            type: 'timestamp',
            timestamp: str
        };
    }

    throw new Error(`Could not decode temporal string: ${str}`);
}

function intervalArithmetic(type) {
    return function(node) {
        const [ spanArg, intervalArg ] = node.args;
        const span = parseInt(spanArg);
        if (isNaN(span)) {
            throw new Error(`Non-numeric span: ${spanArg}`);
        }
        const interval = intervalMap[intervalArg];
        if (!interval) {
            throw new Error(`Unrecognized interval: ${intervalArg}`);
        }
        return {
            type,
            lh: {
                type: 'fn',
                fn: 'now'
            },
            rh: {
                type: 'interval',
                interval,
                span
            }
        }
    };
}

function relToToday(type) {
    return function() {
        return {
            type,
            lh: { type: 'current_date' },
            rh: {
                type: 'interval',
                interval: 'day',
                span: 1
            }
        }
    };
}

function isInCurrentWeek(expr) {
    const currentDayOfWeek = {
        type: 'extract',
        field: 'dow',
        timestamp: { type: 'current_date' }
    };

    const firstOfCurrentWeek = {
        type: 'sub',
        lh: { type: 'current_date' },
        rh: {
            type: 'cast',
            expr: {
                type: 'concat',
                args: [ currentDayOfWeek, ' days' ]
            },
            datatype: 'interval'
        }
    };

    const firstOfNextWeek = {
        type: 'add',
        lh: { type: 'current_date' },
        rh: {
            type: 'cast',
            expr: {
                type: 'concat',
                args: [
                    {
                        type: 'sub',
                        lh: 7,
                        rh: currentDayOfWeek,
                    },
                    ' days'
                ]
            },
            datatype: 'interval'
        }
    };

    return {
        type: 'and',
        args: [
            {
                type: 'gte',
                lh: expr,
                rh: firstOfCurrentWeek
            },
            {
                type: 'lt',
                lh: expr,
                rh: firstOfNextWeek
            }
        ]
    };
}

function convertTemporalFunctions(_schema, query) {
    return preorderReplace(query, when(
        isFnType('fixed'), node => strToTemporalNode(node.args[0]),
        isFnType('afternow'), intervalArithmetic('add'),
        isFnType('beforenow'), intervalArithmetic('sub'),
        isFnType('today'), () => ({ type: 'current_date' }),
        isFnType('yesterday'), relToToday('sub'),
        isFnType('tomorrow'), relToToday('add'),
        isFnType('inThisWeek'), node => isInCurrentWeek(node.args[0])
    ));
}

module.exports = convertTemporalFunctions;