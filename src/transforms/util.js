function isNodeType(type) {
    return n => n && n.type === type;
}

function isFnType(fn) {
    return n => n && n.fn === fn;
}

function chain(...xforms) {
    return function(schema, query) {
        for (const xform of xforms) {
            query = xform(schema, query);
        }

        return query;
    };
}

module.exports = {
    isNodeType,
    isFnType,
    chain
};