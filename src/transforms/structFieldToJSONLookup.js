const { when, preorderReplace } = require('../util');
const { isNodeType } = require('./util');

function asJSON(field, path, property, quote) {
    const node = {
        type: 'jsonprop',
        path,
        property
    };

    if (quote) {
        node.quote = quote;
    }

    const embeddedField = field.lookupEmbedded(property);
    if (embeddedField && !embeddedField.isStringLike) {
        node.datatype = embeddedField && embeddedField.type;
    }

    return node;
}

function structFieldToJSONLookup(schema, query) {
    const convert = (node) => {
        const { path, quote } = node;
        const [ tableName, columnName, ...propsPath ] = path;
        if (propsPath.length === 0) {
            return node;
        }
        const collection = schema.collectionForTable(tableName);
        const field = collection.fieldForColumn(columnName);

        if (!field) {
            console.error(node);
            throw new Error(`No column ${columnName} on ${tableName}`)
        }
        if (field.type === 'struct') {
            return asJSON(field, [tableName, columnName], propsPath, quote);
        }

        return node;
    }

    return preorderReplace(query, when(
        isNodeType('path'), convert
    ));
}

module.exports = structFieldToJSONLookup;