const addFrom = require('./addFrom');
const addJoins = require('./addJoins');
const addGroupBy = require('./addGroupBy');
const annotateColumnsWithCollections = require('./annotateColumnsWithCollections');
const convertBoolOpPredToAggFn = require('./convertBoolOpPredToAggFn');
const convertTemporalFunctions = require('./convertTemporalFunctions');
const convertAggregationToFunction = require('./convertAggregationToFunction');
const convertFieldToPath = require('./convertFieldToPath');
const convertPredToNode = require('./convertPredToNode');
const inferTargetCollection = require('./inferTargetCollection');
const inputToQuery = require('./inputToQuery');
const structFieldToJSONLookup = require('./structFieldToJSONLookup');
const validateQuery = require('./validateQuery');
// const tap = require('./_tap');
const { chain } = require('./util');

module.exports = chain(
    inputToQuery,
    inferTargetCollection,
    annotateColumnsWithCollections,
    // tap,
    convertPredToNode,
    convertAggregationToFunction,
    convertFieldToPath,
    convertBoolOpPredToAggFn,
    convertTemporalFunctions,
    structFieldToJSONLookup,
    addFrom,
    addJoins,
    addGroupBy,
    // tap,
    validateQuery
);