const { and, collect, preorderTraverse, when } = require('../util');
const { isNodeType } = require('./util');

const isPath = isNodeType('path');
const isSubquery = isNodeType('subquery');
const isAggregationOfPath = and(
    isNodeType('aggFn'),
    ({ expr }) => isNodeType('path')(expr)
);

function inferTargetCollection(schema, query) {
    if (query._target) {
        return query;
    }

    const qualifiedColumns = collect(query, isPath, isSubquery);
    const aggregatedColumns = collect(query, isAggregationOfPath)
        .map(node => node.expr);
    const nonAggregatedTables = qualifiedColumns
        .filter(col => aggregatedColumns.indexOf(col) === -1)
        .map(({ path }) => path[0]);

    const sortedTables = schema.orderedCollections // Get hierarchy of collections
        .reverse()                                 // Sort from lowest to highest
        .map(coll => coll.table);                  // Extract the tables

    let target = sortedTables.find(tbl => nonAggregatedTables.indexOf(tbl) !== -1);
    if (!target) {
        const tables = qualifiedColumns.map(({ path }) => path[0]);
        target = sortedTables.find(tbl => tables.indexOf(tbl) !== -1);
    }
    query._target = target;

    preorderTraverse(query,
        when(
            and(isSubquery, subquery => subquery !== query),
            subquery => inferTargetCollection(schema, subquery)
        ),
        isSubquery
    );

    return query;
}

module.exports = inferTargetCollection;