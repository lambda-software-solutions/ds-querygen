const { collect, or } = require('../util');
const { isNodeType } = require('./util');

const isAliasNode = isNodeType('alias');
const isAggFnNode = isNodeType('aggFn');

function addGroupBy(_schema, query) {
    const aggregations = collect(query.select, isAggFnNode);
    if (aggregations.length === 0) {
        return query;
    }

    const dimensions = query.select.exprs
        .filter(expr => collect(expr, isAggFnNode).length === 0)
        .map(node =>
            isAliasNode(node) ?
            { type: 'ident', val: node.alias } :
            node
        );

    if (dimensions.length > 0) {
        query.group = {
            type: "groupby",
            dimensions
        };
    }

    return query;
}

module.exports = addGroupBy;