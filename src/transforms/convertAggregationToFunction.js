const { when, preorderReplace } = require('../util');
const { isNodeType } = require('./util');

// Gets the attributes of a node that belong on the inner expression node
// rather than the outer "aggFn" node
function innerAttrs(node) {
    return Object.keys(node).reduce((acc, key) => {
        if (key !== 'type' && key !== 'aggregation') {
            acc[key] = node[key];
        }

        return acc;
    }, {});
}

function innerExpr(node) {
    if (typeof node.path === 'string') {
        return Object.assign(innerAttrs(node), { type: 'column' });
    }

    throw new Error('Could not infer inner expression of aggregation node: ' + JSON.stringify(node));
}

function aggregationToFunction(node) {
    return {
        type: 'aggFn',
        aggFn: node.aggregation.value,
        expr: innerExpr(node)
    };
}

function convertFieldToPath(_schema, query) {
    return preorderReplace(query, when(
        isNodeType('aggregation'), aggregationToFunction
    ));
}

module.exports = convertFieldToPath;