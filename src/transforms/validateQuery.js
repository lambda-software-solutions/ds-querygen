const validate = require('../validators/index');

function validateQuery(schema, query) {
    const errors = validate(schema, query);
    if (errors.length > 0) {
        throw new Error(`Query failed validation: ${errors.join('; ')}`);
    }

    return query;
}

module.exports = validateQuery;