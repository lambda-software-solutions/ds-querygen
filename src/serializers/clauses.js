const { withValidation, isRequired } = require('./util');

function serialzeSQLClause(label, lst, serialize) {
    return `${label} ${lst.map(serialize).join(', ')}`;
}

const serializeSelect = withValidation(isRequired('exprs'), (node, serialize) => {
    return serialzeSQLClause('SELECT', node.exprs, serialize);
});

const serializeFrom = withValidation(isRequired('tables'), (node, serialize) => {
    return serialzeSQLClause('FROM', node.tables, serialize);
});

const serializeJoin = withValidation(isRequired('table', 'expr'), (node, serialize) => {
    const { table, joinType, expr } = node;
    const modifier = joinType ? `${joinType.toUpperCase()} ` : '';
    return `${modifier}JOIN ${serialize(table)} ON ${serialize(expr)}`;
});

const serializeJoins = withValidation(isRequired('joins'), (node, serialize) => {
    const { joins } = node;
    return joins.map(serialize).join(' ');
});

const serializeWhere = withValidation(isRequired('expr'), (node, serialize) => {
    return `WHERE ${serialize(node.expr)}`;
});

const serializeOrder = withValidation(isRequired('expr', 'dir'), (node, serialize) => {
    const { expr, dir } = node;
    return `${serialize(expr)} ${dir.toUpperCase()}`;
});

const serializeOrderBy = withValidation(isRequired('orders'), (node, serialize) => {
    return serialzeSQLClause('ORDER BY', node.orders, serialize);
});

const serializeGroupBy = withValidation(isRequired('dimensions'), (node, serialize) => {
    return serialzeSQLClause('GROUP BY', node.dimensions, serialize);
});

const serializeLimit = withValidation(isRequired('limit'), (node) => {
    const { limit, offset } = node;
    let out = `LIMIT ${limit}`;

    if (offset) {
        out += ` OFFSET ${offset}`;
    }

    return out;
});

function registerAll(serializer) {
    serializer.register('select', serializeSelect);
    serializer.register('from', serializeFrom);
    serializer.register('joins', serializeJoins);
    serializer.register('where', serializeWhere);
    serializer.register('orderby', serializeOrderBy);
    serializer.register('groupby', serializeGroupBy);
    serializer.register('limit', serializeLimit);

    serializer.register('join', serializeJoin);
    serializer.register('order', serializeOrder);
}

module.exports = registerAll;