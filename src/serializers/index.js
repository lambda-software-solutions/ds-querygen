const registerClauses = require('./clauses');
const registerExpressions = require('./expressions');
const registerFunctions = require('./functions');
const registerIdentifiers = require('./identifiers');
const registerModifiers = require('./modifiers');
const registerPrimitives = require('./primitives');
const registerQueries = require('./queries');

function registerAll(serializer) {
    registerClauses(serializer);
    registerExpressions(serializer);
    registerFunctions(serializer);
    registerIdentifiers(serializer);
    registerModifiers(serializer);
    registerPrimitives(serializer);
    registerQueries(serializer);
}

module.exports = registerAll;