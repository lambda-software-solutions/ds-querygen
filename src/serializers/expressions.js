const { withValidation, isRequired } = require('./util');

const hasVal = isRequired('val');
const hasLeftRight = isRequired('lh', 'rh');
const hasArgs = isRequired('args');

function serializeUnaryOp(node, serialize, op) {
    const { val } = node;
    return `${op} ${serialize(val)}`;
}

function serializeBinaryOp(node, serialize, op) {
    const { lh, rh } = node;
    return `${serialize(lh)} ${op} ${serialize(rh)}`;
}

function serializeNaryOp(node, serialize, op) {
    const { args } = node;
    return '(' + args.map(serialize).join(` ${op} `) + ')';
}

function serializeFnCall(node, serialize, fn, argFields) {
    let fnArgs = argFields.map(argField => serialize(node[argField]));

    return `${fn}(${fnArgs})`;
}

const serializeNot = withValidation(hasVal, (node, serialize) => {
    return serializeUnaryOp(node, serialize, 'NOT');
});

const serializeAnd = withValidation(hasArgs, (node, serialize) => {
    return serializeNaryOp(node, serialize, 'AND');
});

const serializeOr = withValidation(hasArgs, (node, serialize) => {
    return serializeNaryOp(node, serialize, 'OR');
});

const serializeEqual = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '=');
});

const serializeNotEqual = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '<>');
});

const serializeGreaterThan = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '>');
});

const serializeGreaterThanEqual = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '>=');
});

const serializeLessThan = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '<');
});

const serializeLessThanEqual = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '<=');
});

const serializeBetween = withValidation(isRequired('lh', 'min', 'max'), (node, serialize) => {
    const { lh, min, max } = node;
    return `${serialize(lh)} BETWEEN ${serialize(min)} AND ${serialize(max)}`;
});

const serializeInList = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, 'IN');
});

const serializeNotInList = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, 'NOT IN');
});

const serializePlus = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '+');
});

const serializeMinus = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '-');
});

const serializeMultiply = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '*');
});

const serializeDivide = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '/');
});

const serializeModulus = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '%');
});

const serializeExponent = withValidation(hasLeftRight, (node, serialize) => {
    return serializeBinaryOp(node, serialize, '^');
});

const serializeSquareRoot = withValidation(hasVal, (node, serialize) => {
    return serializeUnaryOp(node, serialize, '|/');
});

const serializeAbsoluteValue = withValidation(hasVal, (node, serialize) => {
    return serializeUnaryOp(node, serialize, '@');
});

const serializeConcat = withValidation(hasArgs, (node, serialize) => {
    return serializeNaryOp(node, serialize, '||');
});

const serializeStrlen = withValidation(hasVal, (node, serialize) => {
    return serializeFnCall(node, serialize, 'char_length', ['val']);
});

const serializeLower = withValidation(hasVal, (node, serialize) => {
    return serializeFnCall(node, serialize, 'lower', ['val']);
});

const serializeUpper = withValidation(hasVal, (node, serialize) => {
    return serializeFnCall(node, serialize, 'upper', ['val']);
});

const serializeSubstring = withValidation(isRequired('val', 'len'), (node, serialize) => {
    const { val, start = 1, len } = node;
    return `substring(${serialize(val)} from ${start} for ${len})`
});

const serializeLike = withValidation(hasLeftRight, (node, serialize) => {
    const { lh, rh, i = false } = node;
    return `${serialize(lh)} ${i ? 'I' : ''}LIKE ${serialize(rh)}`;
});

const serializeMatchRe = withValidation(hasLeftRight, (node, serialize) => {
    const { lh, rh, i = false } = node;
    return `${serialize(lh)} ~${i ? '*' : ''} ${serialize(rh)}`;
});

const serializeNotMatchRe = withValidation(hasLeftRight, (node, serialize) => {
    const { lh, rh, i = false } = node;
    return `${serialize(lh)} !~${i ? '*' : ''} ${serialize(rh)}`;
});

const serializeContains = withValidation(hasLeftRight, (node, serialize) => {
    const { lh, rh, i = false } = node;
    return `${serialize(lh)} ${i ? 'I' : ''}LIKE ${serialize('%' + rh + '%')}`;
});

const serializeStartsWith = withValidation(hasLeftRight, (node, serialize) => {
    const { lh, rh, i = false } = node;
    return `${serialize(lh)} ${i ? 'I' : ''}LIKE ${serialize(rh + '%')}`;
});

const serializeExtract = withValidation(isRequired('field'), (node, serialize) => {
    const { field, timestamp, interval } = node;
    let from;
    if (timestamp) {
        from = (typeof timestamp === 'object') ?
            serialize(timestamp) :
            `TIMESTAMP '${timestamp}'`;
    } else {
        from = `INTERVAL '${interval}'`;
    }
    return `EXTRACT(${field.toUpperCase()} FROM ${from})`;
});

function serializeCurrentDate(_node, _serialize) {
    return 'CURRENT_DATE';
}

const serializeCast = withValidation(isRequired('expr', 'datatype'), (node, serialize) => {
    const { expr, datatype } = node;
    return `CAST(${serialize(expr)} AS ${datatype.toUpperCase()})`;
});

function serializeNull(_node, _serialize) {
    return 'NULL';
}

function serializeIsNull(node, serialize) {
    return `${serialize(node.expr)} IS NULL`;
}

function serializeIsNotNull(node, serialize) {
    return `${serialize(node.expr)} IS NOT NULL`;
}

const serializeExists = withValidation(isRequired('subquery'), (node, serialize) => {
    return `EXISTS ${serialize(node.subquery)}`;
});

function registerAll(serializer) {
    // Logical
    serializer.register('not', serializeNot);
    serializer.register('and', serializeAnd);
    serializer.register('or', serializeOr);

    // Comparison
    serializer.register('eq', serializeEqual, { aliases: ['='] });
    serializer.register('neq', serializeNotEqual, { aliases: ['!=', '<>'] });
    serializer.register('gt', serializeGreaterThan, { aliases: ['>'] });
    serializer.register('gte', serializeGreaterThanEqual, { aliases: ['>='] });
    serializer.register('lt', serializeLessThan, { aliases: ['<'] });
    serializer.register('lte', serializeLessThanEqual, { aliases: ['<='] });
    serializer.register('between', serializeBetween);
    serializer.register('inlist', serializeInList);
    serializer.register('ninlist', serializeNotInList, { aliases: ['notinlist'] });

    // Mathematical
    serializer.register('add', serializePlus);
    serializer.register('sub', serializeMinus);
    serializer.register('mlt', serializeMultiply);
    serializer.register('div', serializeDivide);
    serializer.register('mod', serializeModulus);
    serializer.register('exp', serializeExponent);
    serializer.register('sqrt', serializeSquareRoot);
    serializer.register('abs', serializeAbsoluteValue);

    // String
    serializer.register('concat', serializeConcat);
    serializer.register('strlen', serializeStrlen);
    serializer.register('lower', serializeLower);
    serializer.register('upper', serializeUpper);
    serializer.register('substr', serializeSubstring);
    serializer.register('like', serializeLike);
    serializer.register('match', serializeMatchRe);
    serializer.register('nmatch', serializeNotMatchRe);
    serializer.register('contains', serializeContains);
    serializer.register('startsWith', serializeStartsWith);

    // Date and time
    serializer.register('extract', serializeExtract);
    serializer.register('current_date', serializeCurrentDate);

    // Types
    serializer.register('cast', serializeCast);

    // Null
    serializer.register('null', serializeNull);
    serializer.register('isNull', serializeIsNull);
    serializer.register('isNotNull', serializeIsNotNull);

    // Subquery expressions
    serializer.register('exists', serializeExists);
}

module.exports = registerAll;