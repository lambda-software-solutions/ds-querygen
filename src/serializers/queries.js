const { withValidation, isRequired } = require('./util');

const serializeQuery = withValidation(isRequired('select'), (node, serialize) => {
    const { select, from, joins, where, order, group, limit } = node;
    let out = serialize(select);
    if (from) {
        out += ' ' + serialize(from);
    }
    if (joins) {
        out += ' ' + serialize(joins);
    }
    if (where) {
        out += ' ' + serialize(where);
    }
    if (group) {
        out += ' ' + serialize(group);
    }
    if (order) {
        out += ' ' + serialize(order);
    }
    if (limit) {
        out += ' ' + serialize(limit);
    }

    return out;
});

function serializeSubQuery(node, serialize) {
    return '(' + serialize(Object.assign({}, node, { type: 'query' })) + ')';
}

function registerAll(serializer) {
    serializer.register('subquery', serializeSubQuery);
    serializer.register('query', serializeQuery);
}

module.exports = registerAll;