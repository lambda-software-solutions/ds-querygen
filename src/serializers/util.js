function withValidation(validate, serializer) {
    return (node, serialize) => {
        const errors = validate(node);
        if (errors && errors.length > 0) {
            throw new Error(`Error validating node: ${errors.join('; ')} (path=${'TODO'})`);
        }
        return serializer(node, serialize);
    }
}

function isRequired(...props) {
    return (node) => {
        let errors = [];
        for (let prop of props) {
            if (typeof node[prop] === 'undefined') {
                errors.push(`missing required property, ${prop}`);
            }
        }
        return errors;
    }
}

module.exports = {
    withValidation,
    isRequired
};