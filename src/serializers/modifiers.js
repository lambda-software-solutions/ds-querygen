const { withValidation, isRequired } = require('./util');

const serializeAlias = withValidation(isRequired('expr', 'alias'), (node, serialize) => {
    const { expr, alias } = node;
    return `${serialize(expr)} AS ${alias}`;
});

function registerAll(serializer) {
    serializer.register('alias', serializeAlias);
}

module.exports = registerAll;