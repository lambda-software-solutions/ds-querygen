const { withValidation, isRequired } = require('./util');

const serializeFunction = withValidation(isRequired('fn'), (node, serialize) => {
    const { fn, args = [] } = node;
    return `${fn.toUpperCase()}(${args.map(serialize).join(',')})`;
});

const serializeAggregation = withValidation(isRequired('aggFn', 'expr'), (node, serialize) => {
    const { aggFn, expr } = node;
    return `${aggFn.toUpperCase()}(${serialize(expr)})`;
});

function registerAll(serializer) {
    serializer.register('fn', serializeFunction);
    serializer.register('aggFn', serializeAggregation);
}

module.exports = registerAll;