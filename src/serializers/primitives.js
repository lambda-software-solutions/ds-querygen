const { withValidation, isRequired } = require('./util');
const Serializer = require('../Serializer');
const { Primitives } = Serializer;
const { sanitizeString } = require('../util');

function serializeString(s) {
    return `'${sanitizeString(s)}'`;
}

function serializeNumber(n) {
    return n + '';
}

function serializeBoolean(b) {
    return b ? 'TRUE' : 'FALSE';
}

function serializeArray(xs, serialize) {
    return `(${xs.map(serialize).join(', ')})`;
}

const serialzeUUID = withValidation(isRequired('uuid'), (node, _serialize) => {
    const { uuid } = node;
    return `'${uuid}'`;
});

const serializeTimestamp = withValidation(isRequired('timestamp'), (node, _serialize) => {
    const { timestamp } = node;
    return `TIMESTAMP '${timestamp}'`;
});

const serializeDate = withValidation(isRequired('date'), (node, _serialize) => {
    const { date } = node;
    return `DATE '${date}'`;
});

const serializeInterval = withValidation(isRequired('interval', 'span'), (node, _serialize) => {
    const { interval, span } = node;
    return `INTERVAL '${span} ${interval.toUpperCase()}'`;
});

function registerAll(serializer) {
    serializer.register(Primitives.STRING, serializeString);
    serializer.register(Primitives.NUMBER, serializeNumber);
    serializer.register(Primitives.BOOLEAN, serializeBoolean);
    serializer.register(Primitives.ARRAY, serializeArray);

    serializer.register('uuid', serialzeUUID);

    serializer.register('timestamp', serializeTimestamp);
    serializer.register('date', serializeDate);
    serializer.register('interval', serializeInterval);
}

module.exports = registerAll;
