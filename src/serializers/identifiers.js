const { withValidation, isRequired } = require('./util');
const { sanitizeString } = require('../util');

const serializeIdentifier = withValidation(isRequired('val'), (node) => {
    const { val, quote = false } = node;
    if (quote && val !== '*') {
        return '"' + sanitizeString(val, '"') + '"';
    }
    return val;
});

const serializePath = withValidation(isRequired('path'), (node) => {
    const { path, quote = false } = node;
    return path.map(val => serializeIdentifier({ val, quote }))
        .join('.');
});

const serializeJSONProp = withValidation(isRequired('path', 'property'), (node) => {
    const { path, property, datatype = null, quote = false } = node;
    const pathPart = path.map(val => serializeIdentifier({ val, quote })).join('.');
    const segmentCount = property.length;

    let out = pathPart;
    for (let i = 0; i < segmentCount; i++) {
        const segment = property[i];
        const connector = (i < segmentCount - 1) ? '->' : '->>';

        out += ` ${connector} '${sanitizeString(segment)}'`;
    }

    if (datatype) {
        out = `CAST(${out} AS ${datatype})`;
    }

    return out;
});

function registerAll(serializer) {
    serializer.register('ident', serializeIdentifier);
    serializer.register('path', serializePath);
    serializer.register('jsonprop', serializeJSONProp);
}

module.exports = registerAll;