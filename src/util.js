function sanitizeString(s, quoteChar = '\'') {
    let out = '';

    for (let i = 0; i < s.length; i++) {
        const c = s.charAt(i);
        switch (c) {
        case quoteChar:
            out += quoteChar + quoteChar;
            break;
        case '\\':
            out += '\\\\';
            break;
        case '\b':
            out += '\\b';
            break;
        case '\f':
            out += '\\f';
            break;
        case '\n':
            out += '\\n';
            break;
        case '\r':
            out += '\\r';
            break;
        case '\t':
            out += '\\t';
            break;
        default:
            out += c;
        }
    }

    return out;
}

function constantly(val) {
    return () => val;
}

function preorderTraverse(node, visit, shouldTerminate = constantly(false)) {
    visit(node);
    if (shouldTerminate(node)) {
        return;
    }

    if (typeof node == 'object' && node !== null && node.constructor == Object) {
        for (let prop in node) {
            if (node.hasOwnProperty(prop)) {
                preorderTraverse(node[prop], visit, shouldTerminate);
            }
        }
    } else if (Array.isArray(node)) {
        for (let i = 0; i < node.length; i++) {
            preorderTraverse(node[i], visit, shouldTerminate);
        }
    }
}

function preorderReplace(node, update, shouldTerminate = constantly(false)) {
    node = update(node);
    if (shouldTerminate(node)) {
        return node;
    }

    if (typeof node == 'object' && node !== null && node.constructor == Object) {
        for (let prop in node) {
            if (node.hasOwnProperty(prop)) {
                node[prop] = update(node[prop]);
                preorderReplace(node[prop], update, shouldTerminate);
            }
        }
    } else if (Array.isArray(node)) {
        for (let i = 0; i < node.length; i++) {
            node[i] = update(node[i]);
            preorderReplace(node[i], update, shouldTerminate);
        }
    }

    return node;
}

function collect(node, pred, shouldTerminate) {
    let res = [];
    preorderTraverse(node, n => {
        if (pred(n)) {
            res.push(n);
        }
    }, shouldTerminate);

    return res;
}

/**
 * Walks an object tree, `node`, and collects elements satisfying `pred`.
 * Unlike the `collect()` function, this function will stop walking a
 * branch of the tree once it collects a node.
 *
 * @param {Object} node Root of tree to traverse
 * @param {Function} pred Predicate function determining whether to collect the current node
 */
function collectFirst(node, pred) {
    let res = [];
    preorderTraverse(node, n => {
        if (pred(n)) {
            res.push(n);
        }
    }, pred);

    return res;
}

function when(pred, fn, ...otherClauses) {
    if (otherClauses.length % 2 !== 0) {
        throw new Error('Malformed when expression - odd numer of arguments supplied');
    }

    return x => {
        if (pred(x)) {
            return fn(x);
        }

        if (otherClauses.length > 0) {
            const [nextPred, nextFn, ...nextClauses] = otherClauses;
            return when(nextPred, nextFn, ...nextClauses)(x);
        }

        return x;
    };
}

function and(...fns) {
    return (...args) => {
        for (const fn of fns) {
            if (!fn(...args)) {
                return false;
            }
        }

        return true;
    }
}

function or(...fns) {
    return (...args) => {
        for (const fn of fns) {
            if (fn(...args)) {
                return true;
            }
        }

        return false;
    }
}

function groupBy(xs, fn) {
    return xs.reduce((acc, x) => {
        const key = fn(x);
        if (acc[key]) {
            acc[key].push(x);
        } else {
            acc[key] = [x];
        }

        return acc;
    }, []);
}

function butLast(xs) {
    return xs.slice(0, xs.length - 1);
}

function last(xs) {
    return xs[xs.length - 1];
}

module.exports = {
    sanitizeString,
    preorderReplace,
    preorderTraverse,
    collect,
    collectFirst,
    when,
    and,
    or,
    groupBy,
    butLast,
    last
};