const Primitives = {
    NUMBER: Symbol('number'),
    STRING: Symbol('string'),
    BOOLEAN: Symbol('boolean'),
    ARRAY: Symbol('array')
};

function nodeType(node) {
    switch (typeof node) {
        case 'object': return Array.isArray(node) ? Primitives.ARRAY : node.type;
        case 'number': return Primitives.NUMBER;
        case 'string': return Primitives.STRING;
        case 'boolean': return Primitives.BOOLEAN;
        default: throw new Error(`Unknown node type: ${typeof node}`);
    }
}

class Serializer {

    constructor() {
        this.typeSerializers = new Map();
        this.serialize = this.serialize.bind(this);
    }

    static get Primitives() {
        return Primitives;
    }

    register(nodeType, serializer, opts = {}) {
        if (this.typeSerializers[nodeType]) {
            console.warn(`Overwriting existing serializer for type, ${nodeType}`);
        }
        this.typeSerializers.set(nodeType, serializer);

        if (opts.aliases) {
            const otherOpts = Object.assign({}, opts);
            delete otherOpts.aliases;
            opts.aliases.forEach(alias => {
                this.register(alias, serializer, otherOpts);
            });
        }
    }

    serialize(node) {
        let type = nodeType(node);

        if (!this.typeSerializers.has(type)) {
            throw new Error(`No serialzer for type, ${type}`);
        }

        return this.typeSerializers.get(type)(node, this.serialize);
    }
}


module.exports = Serializer;