const { collect } = require('../util');

function allCollectionsValid(schema, query) {
    let errors = [];
    const schemaCollections = new Set(schema.collections.map(c => c.table));
    if (!schemaCollections.has(query._target)) {
        errors.push(`Query target not found in schema: ${query._target}`);
    }

    collect(query, node => node && node._collection)
        .map(node => node._collection)
        .filter(collection => !schemaCollections.has(collection))
        .forEach(collection => errors.push(`Query collection not found in schema: ${collection}`));

    return errors;
}

module.exports = allCollectionsValid;