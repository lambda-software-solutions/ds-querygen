const allCollectionsValid = require('./allCollectionsValid');
const allColumnsValid = require('./allColumnsValid');

function validate(schema, query) {
    return [
        ...allCollectionsValid(schema, query),
        ...allColumnsValid(schema, query)
    ];
}

module.exports = validate;
