const { collect, last } = require('../util');

function allColumnsValid(schema, query) {
    let errors = [];

    collect(query, node => node && node.type === 'path', node => node && node.type === 'subquery')
        .filter(node => !!node._collection) // Some paths, such as in the from clause, may not be annotated
        .forEach(node =>  {
            const { _collection, path } = node;
            const columnName = last(path);
            // FIXME: this rule is not restrictive enough, as wildcards should only be allowed in the selection
            if (columnName === '*') {
                return;
            }
            const collection = schema.collectionForTable(_collection);
            const field = collection.fieldForColumn(columnName);

            if (!field) {
                errors.push(`Column ${columnName} does not exist on collection ${_collection}`);
            }
        });

    return errors;
}

module.exports = allColumnsValid;