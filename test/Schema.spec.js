const { expect } = require('chai');
const Schema = require('../src/Schema');

const schemaDef = () => ({
    version: '0.0.1-test',
    collections: [
        {
            title: 'Orders',
            table: 'order',
            fields: [
                { column: 'id', type: 'int', title: 'ID' }
            ],
            relationships: [
                { type: 'hasMany', collection: 'service_item' }
            ]
        },
        {
            title: 'Service Items',
            table: 'service_item',
            fields: [
                { column: 'id', type: 'int', title: 'ID' },
                { column: 'order_id', type: 'int', title: 'ID' },
            ],
            relationships: [
                { type: 'belongsTo', collection: 'order' }
            ]
        }
    ]
});

describe('Schema', () => {
    let s;

    beforeEach(() => {
        s = Schema.from(schemaDef());
    });

    it('Should create an instance', () => {
        expect(s).to.be.an.instanceOf(Schema);
    });

    it('Should get the version', () => {
        expect(s.version).to.equal('0.0.1-test');
    });

    it('should get collections', () => {
        expect(s.collections.length).to.equal(2);
        expect(s.collectionForTable('order').title).to.equal('Orders');
    });

    it('should throw when trying to get a nonexistent collection', () => {
        expect(() => s.collectionForTable('bogus')).to.throw();
    });

    it('should get an ordered list of collections', () => {
        expect(s.orderedCollections).to.eql([
            s.collectionForTable('order'),
            s.collectionForTable('service_item')
        ]);
    });

    it('should create one relationship link for each pair of related collections', () => {
        const rel = s.relationships[0];
        expect(s.relationships.length).to.equal(1);
        expect(rel.left).to.equal('order');
        expect(rel.right).to.equal('service_item');
    });

    it('should indicate a multiple-cardinality relationship', () => {
        expect(s.relationships[0].cardinality).to.equal(Schema.CARDINALITY.MANY);
    });

    it('should indicate a single-cardinality relationship', () => {
        const schemaJSON = schemaDef();
        schemaJSON.collections[0].relationships[0].type = 'hasOne';
        schemaJSON.collections[1].relationships = [];
        s = Schema.from(schemaJSON);
        expect(s.relationships[0].cardinality).to.equal(Schema.CARDINALITY.ONE);
    });

    it('should throw an error when only a belongsTo relationship is defined', () => {
        const schemaJSON = schemaDef();
        schemaJSON.collections[0].relationships = [];

        expect(() => Schema.from(schemaJSON)).to.throw();
    });

    it('should not care in which order the relationships are defined', () => {
        const schemaJSON = schemaDef();
        const tmpColl = schemaJSON.collections[0];
        schemaJSON.collections[0] = schemaJSON.collections[1];
        schemaJSON.collections[1] = tmpColl;
        s = Schema.from(schemaJSON);
        const rel = s.relationships[0];

        expect(rel.left).to.equal('order');
        expect(rel.right).to.equal('service_item');
        expect(s.relationships[0].cardinality).to.equal(Schema.CARDINALITY.MANY);
    });

    it('should throw an error on an incorrect relationship type', () => {
        const schemaJSON = schemaDef();
        schemaJSON.collections[0].relationships[0].type = 'has🌮';

        expect(() => Schema.from(schemaJSON)).to.throw();
    });
});