import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import validate from '../../src/validators/allColumnsValid';
import xform from '../../src/transforms/annotateColumnsWithCollections';

describe('Validating all paths are valid', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should consider a query with all paths correctly rooted to be valid', () => {
        const query = xform(schema, {
            type: 'query',
            _target: 'billing',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['billing', 'id'] },
                ]
            }
        });

        const errors = validate(schema, query);
        expect(errors).to.be.empty;
    });

    it('should consider a query that has a non-existent column to be invalid', () => {
        const query = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'ident', val: 'bogus_col' }
                ]
            }
        });

        const errors = validate(schema, query);
        expect(errors.length).to.equal(1);
        expect(errors[0]).to.contain('Column bogus_col does not exist on collection order');
    });

    it('should allow a wildcard as a column in the selection', () => {
        const query = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'ident', val: '*' }
                ]
            }
        });

        const errors = validate(schema, query);
        expect(errors).to.be.empty;
    });
});