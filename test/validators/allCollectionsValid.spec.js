import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import validate from '../../src/validators/allCollectionsValid';
import annotateColumnsWithCollections from '../../src/transforms/annotateColumnsWithCollections';

const xform = annotateColumnsWithCollections;

describe('Validating all collections are valid', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should consider a query with only schema collections to be valid', () => {
        const query = xform(schema, {
            type: 'query',
            _target: 'billing',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'ident', val: 'id' },
                ]
            }
        });

        const errors = validate(schema, query);
        expect(errors).to.be.empty;
    });

    it('should consider a query that has a non-schema collection in its selection to be invalid', () => {
        const query = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'ident', val: 'id' },
                    { type: 'path', path: ['frombule', 'id'] },
                ]
            }
        });

        const errors = validate(schema, query);
        expect(errors.length).to.equal(1);
        expect(errors[0]).to.contain('frombule');
    });
});