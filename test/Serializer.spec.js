const { expect } = require('chai');
const Serializer = require('../src/Serializer');

const { Primitives } = Serializer;

describe('Serialization', () => {

    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
    });

    it('Should allow a serializer to be registered for a type', () => {
        serializer.register('foo', (_node, _serialize) => 'I am a foo');

        expect(serializer.serialize({ type: 'foo' })).to.equal('I am a foo');
    });

    it('should allow aliases to be specified for a node type', () => {
        serializer.register('foo', (node, _serialize) => `Foo called as: ${node.type}`, { aliases: ['bar', 'baz'] });

        expect(serializer.serialize({ type: 'foo' })).to.equal('Foo called as: foo');
        expect(serializer.serialize({ type: 'bar' })).to.equal('Foo called as: bar');
        expect(serializer.serialize({ type: 'baz' })).to.equal('Foo called as: baz');
    });

    it('Should allow a serializer to be registered for primitive types', () => {
        serializer.register(Primitives.NUMBER, (n, _serialize) => `#${n}`);
        serializer.register(Primitives.STRING, (s, _serialize) => `"${s}"`);
        serializer.register(Primitives.BOOLEAN, (v, _serialize) => v ? 'yes' : 'no');

        expect(serializer.serialize(42)).to.equal('#42');
        expect(serializer.serialize('hi')).to.equal('"hi"');
        expect(serializer.serialize(false)).to.equal('no');
    });

    it('shold allow a serializer to be overwritten', () => {
        serializer.register('foo', (_node, _serialize) => 'should not display');
        serializer.register('foo', (_node, _serialize) => 'should display');

        expect(serializer.serialize({ type: 'foo' })).to.equal('should display');
    });

    it('should pass the serialzer into the serialization function for recursive serialization', () => {
        serializer.register('parent', (node, serialize) => `My name is ${node.name}, and my child is ${serialize(node.child)}`);
        serializer.register('child', (node, _serialize) => `Baby ${node.name}`);
        const root = {
            type: 'parent',
            name: 'Albert',
            child: {
                type: 'child',
                name: 'Billy'
            }
        };

        expect(serializer.serialize(root)).to.equal('My name is Albert, and my child is Baby Billy');
    });
});