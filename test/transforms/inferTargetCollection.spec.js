import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/inferTargetCollection';

describe('Inferring a target collection', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('Should infer the target as the only selected collection', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                ]
            },
            filter: null
        });

        expect(newQuery._target).to.equal('order');
    });

    it('Should select the lower of two collections in a hierarchy', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['service_item', 'id'] },
                ]
            },
            filter: null
        });

        expect(newQuery._target).to.equal('service_item');
    });

    it('Should select the lowest non-aggregated collection in a hierarchy', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'aggFn',
                        aggFn: 'sum',
                        expr: { type: 'path', path: ['billing', 'amount'] }
                    },
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['service_item', 'id'] }
                ]
            },
            filter: null
        });

        expect(newQuery._target).to.equal('service_item');
    });

    it('Should select the lowest aggregated collection when no non-aggregated columns are present', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'aggFn',
                        aggFn: 'sum',
                        expr: { type: 'path', path: ['billing', 'amount'] }
                    },
                    {
                        type: 'aggFn',
                        aggFn: 'max',
                        expr: { type: 'path', path: ['service_item', 'id'] }
                    }
                ]
            },
            filter: null
        });

        expect(newQuery._target).to.equal('billing');
    });
});