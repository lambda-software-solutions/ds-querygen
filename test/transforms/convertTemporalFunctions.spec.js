import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/convertTemporalFunctions';

function selectOne(expr) {
    return {
        type: 'query',
        select: {
            type: 'select',
            exprs: [ expr ]
        }
    };
}

describe('Converting temporal fns to date and timestamp expressions', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should convert a fixed timestamp to a TIMESTAMP node', () => {
        const query = xform(schema, selectOne({
            fn: 'fixed',
            args: ['2018-10-10T16:00:00.000Z']
        }));

        expect(query.select.exprs[0]).to.eql({
            type: 'timestamp',
            timestamp: '2018-10-10T16:00:00.000Z'
        });
    });

    it('should convert a fixed date to a DATE node', () => {
        const query = xform(schema, selectOne({
            fn: 'fixed',
            args: ['2018-10-10']
        }));

        expect(query.select.exprs[0]).to.eql({
            type: 'date',
            date: '2018-10-10'
        });
    });

    it('should convert afternow to an add of now and the arg', () => {
        const query = xform(schema, selectOne({
            fn: 'afternow',
            args: [3, 'days']
        }));

        expect(query.select.exprs[0]).to.eql({
            type: 'add',
            lh: {
                type: 'fn',
                fn: 'now'
            },
            rh: {
                type: 'interval',
                interval: 'day',
                span: 3
            }
        });
    });

    it('should convert beforernow to a subtraction of now and the arg', () => {
        const query = xform(schema, selectOne({
            fn: 'beforenow',
            args: [3, 'days']
        }));

        expect(query.select.exprs[0]).to.eql({
            type: 'sub',
            lh: {
                type: 'fn',
                fn: 'now'
            },
            rh: {
                type: 'interval',
                interval: 'day',
                span: 3
            }
        });
    });

    it('should convert today', () => {
        const query = xform(schema, selectOne({ fn: 'today' }));
        expect(query.select.exprs[0]).to.eql({
            type: 'current_date'
        });
    });

    it('should convert yesterday', () => {
        const query = xform(schema, selectOne({ fn: 'yesterday' }));
        expect(query.select.exprs[0]).to.eql({
            type: 'sub',
            lh: {
                type: 'current_date'
            },
            rh: {
                type: 'interval',
                interval: 'day',
                span: 1
            }
        });
    });

    it('should convert tomorrow', () => {
        const query = xform(schema, selectOne({ fn: 'tomorrow' }));
        expect(query.select.exprs[0]).to.eql({
            type: 'add',
            lh: {
                type: 'current_date'
            },
            rh: {
                type: 'interval',
                interval: 'day',
                span: 1
            }
        });
    });

    it('should convert inThisWeek to an inclusion in the half-open range between the previous and next Sundays', () => {
        const someTimestamp = {
            type: 'timestamp',
            timestamp: '2018-10-10T16:00:00.000Z'
        };
        const query = xform(schema, selectOne({
            fn: 'inThisWeek',
            args: [someTimestamp]
        }));

        expect(query.select.exprs[0]).to.eql({
            type: 'and',
            args: [
                {
                    type: 'gte',
                    lh: someTimestamp,
                    rh: {
                        type: 'sub',
                        lh: { type: 'current_date' },
                        rh: {
                            type: 'cast',
                            expr: {
                                type: 'concat',
                                args: [
                                    {
                                        type: 'extract',
                                        field: 'dow',
                                        timestamp: { type: 'current_date' }
                                    },
                                    ' days'
                                ]
                            },
                            datatype: 'interval'
                        }
                    }
                },
                {
                    type: 'lt',
                    lh: someTimestamp,
                    rh: {
                        type: 'add',
                        lh: { type: 'current_date' },
                        rh: {
                            type: 'cast',
                            expr: {
                                type: 'concat',
                                args: [
                                    {
                                        type: 'sub',
                                        lh: 7,
                                        rh: {
                                            type: 'extract',
                                            field: 'dow',
                                            timestamp: { type: 'current_date' }
                                        },
                                    },
                                    ' days'
                                ]
                            },
                            datatype: 'interval'
                        }
                    }
                }
            ]
        });
    });
});