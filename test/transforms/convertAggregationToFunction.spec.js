import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/convertAggregationToFunction';

describe('Aggregation in input to aggFn node', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should convert an aggregation on a path to an aggFn of a path', () => {
        const newQuery = xform(schema, {
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'aggregation',
                        aggregation: {
                            value: 'max'
                        },
                        path: 'service.created_at',
                    }
                ]
            }
        });

        expect(newQuery.select.exprs[0]).to.eql({
            type: 'aggFn',
            aggFn: 'max',
            expr: {
                type: 'column',
                path: 'service.created_at'
            }
        });
    });

    it('should throw an error when the inner expression type can not be inferred', () => {
        const query = {
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'aggregation',
                        aggregation: {
                            value: 'max'
                        },
                        look: 'i have no path'
                    }
                ]
            }
        };

        expect(() => xform(schema, query)).to.throw();
    });
});