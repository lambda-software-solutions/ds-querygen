import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/addGroupBy';

describe('Adding dimension grouping', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('Should not add a group by when no aggregation present', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                ]
            }
        });

        expect(newQuery.group).to.be.undefined;
    });

    it('Should add a group by on all non-aggregated selections when any aggregations are present', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['user', 'email'] },
                    {
                        type: 'aggFn',
                        aggFn: 'max',
                        expr: { type: 'path', path: ['service', 'created_at'] },
                    }
                ]
            }
        });

        expect(newQuery.group).to.eql({
            type: 'groupby',
            dimensions: [
                { type: 'path', path: ['order', 'id'] },
                { type: 'path', path: ['user', 'email'] }
            ]
        });
   });

   it('should use the aliased name in the grouping if supplied', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'alias',
                        alias: 'order_id',
                        expr: { type: 'path', path: ['order', 'id'] }
                    },
                    { type: 'path', path: ['user', 'email'] },
                    {
                        type: 'aggFn',
                        aggFn: 'max',
                        expr: { type: 'path', path: ['service', 'created_at'] },
                    }
                ]
            }
        });

        expect(newQuery.group).to.eql({
            type: 'groupby',
            dimensions: [
                { type: 'ident', val: 'order_id' },
                { type: 'path', path: ['user', 'email'] }
            ]
        });
   });

   it('Should add a group on an expression', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    {
                        type: 'add',
                        rh: { type: 'path', path: ['order', 'id'] },
                        lh: 100
                    },
                    {
                        type: 'aggFn',
                        aggFn: 'max',
                        expr: { type: 'path', path: ['service', 'created_at'] },
                    }
                ]
            }
        });

        expect(newQuery.group).to.eql({
            type: 'groupby',
            dimensions: [
                {
                    type: 'add',
                    rh: { type: 'path', path: ['order', 'id'] },
                    lh: 100
                }
            ]
        });
    });
});