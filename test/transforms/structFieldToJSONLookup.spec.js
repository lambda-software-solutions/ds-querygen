import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/structFieldToJSONLookup';

describe('Converting fields lookups for embedded structs to JSON property lookups', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);

    });

    it('should convert a string-like struct field to a JSON property', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'region', 'name'] },
                ]
            }
        });

        expect(newQuery.select.exprs[0]).to.eql({
            type: 'jsonprop',
            path: ['order', 'region'],
            property: ['name']
        });
    });

    it('should convert a quoted path to a quoted JSON property', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'region', 'name'], quote: true },
                ]
            }
        });

        expect(newQuery.select.exprs[0]).to.eql({
            type: 'jsonprop',
            path: ['order', 'region'],
            property: ['name'],
            quote: true
        });
    });

    it('should convert a non-string field to a typed JSON property', () => {
        const newQuery = xform(schema, {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'region', 'is_active'] },
                ]
            }
        });

        expect(newQuery.select.exprs[0]).to.eql({
            type: 'jsonprop',
            path: ['order', 'region'],
            property: ['is_active'],
            datatype: 'boolean'
        });
    });
});