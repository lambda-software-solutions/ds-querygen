import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/convertBoolOpPredToAggFn';

function filterQuery(expr) {
    return {
        type: 'query',
        where: {
            type: 'where',
            expr
        }
    };
}

describe('Converting boolean operation predicates to predicates over boolean aggregations', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should convert bool_and', () => {
        const query = xform(schema, filterQuery({
            type: 'bool_and',
            lh: {
                type: 'path',
                path: ['service_item', 'is_preview_mode']
            }
        }));

        expect(query.where.expr).to.eql({
            type: 'eq',
            lh: {
                type: 'aggFn',
                aggFn: 'bool_and',
                expr: {
                    type: 'path',
                    path: ['service_item', 'is_preview_mode']
                }
            },
            rh: true
        });
    });

    it('should convert bool_or', () => {
        const query = xform(schema, filterQuery({
            type: 'bool_or',
            lh: {
                type: 'path',
                path: ['service_item', 'is_preview_mode']
            }
        }));

        expect(query.where.expr).to.eql({
            type: 'eq',
            lh: {
                type: 'aggFn',
                aggFn: 'bool_or',
                expr: {
                    type: 'path',
                    path: ['service_item', 'is_preview_mode']
                }
            },
            rh: true
        });
    });
});