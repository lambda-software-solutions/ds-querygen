import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/inputToQuery';

describe('Simplified input to Query AST', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it ('should have an empty default', () => {
        const newQuery = xform(schema, {});
        expect(newQuery).to.eql({
            type: 'query',
            select: {
                type: 'select',
                exprs: []
            }
        });
    });

    it('Should use a specified collection as the target', () => {
        const newQuery = xform(schema, { collection: 'order' });

        expect(newQuery._target).to.eql('order');
    });

    it('should use the specified selection', () => {
        const testSelection = [ 'some_column', 'other_column' ];
        const newQuery = xform(schema, {
            selection: testSelection
        });

        expect(newQuery.select.exprs).to.eql(testSelection);
    });

    it('should use specified columns as selection', () => {
        const testColumns = [ 'some_column', 'other_column' ];
        const newQuery = xform(schema, {
            columns: testColumns
        });

        expect(newQuery.select.exprs).to.eql(testColumns);
    })

    it('should use the specified filters', () => {
        const testFilter = { some: 'filter' };
        const newQuery = xform(schema, {
            filter: testFilter
        });

        expect(newQuery.where).to.eql({
            type: 'where',
            expr: testFilter
        });
    });

    it('should use the specified pagination to build a limit', () => {
        const newQuery = xform(schema, {
            pagination: {
                page: 5,
                perPage: 20
            }
        });

        expect(newQuery.limit).to.eql({
            type: 'limit',
            limit: 20,
            offset: 100
        });
    });

    it('should order by an expression', () => {
        const newQuery = xform(schema, {
            order: {
                expression: { type: 'fn', fn: 'random' }
            }
        });

        expect(newQuery.order).to.eql({
            type: 'orderby',
            orders: [
                {
                    type: 'order',
                    expr: { type: 'fn', fn: 'random' },
                    dir: 'ASC'
                }
            ]
        });
    });

    it('should order by a simple string column', () => {
        const newQuery = xform(schema, {
            order: {
                column: 'id'
            }
        });

        expect(newQuery.order).to.eql({
            type: 'orderby',
            orders: [
                {
                    type: 'order',
                    expr: { type: 'ident', val: 'id', quote: true },
                    dir: 'ASC'
                }
            ]
        });
    });

    it('should order by a segmented string column', () => {
        const newQuery = xform(schema, {
            order: {
                column: 'order.id'
            }
        });

        expect(newQuery.order).to.eql({
            type: 'orderby',
            orders: [
                {
                    type: 'order',
                    expr: { type: 'path', path: ['order', 'id'], quote: true },
                    dir: 'ASC'
                }
            ]
        });
    });

    it('should allow the order direction to be specified', () => {
        const newQuery = xform(schema, {
            order: {
                column: 'id',
                direction: 'DESC'
            }
        });

        expect(newQuery.order.orders[0].dir).to.eql('DESC');
    });

    it('should allow multiple orderings to be specified', () => {
        const newQuery = xform(schema, {
            order: [
                { column: 'name' },
                { column: 'order.id', direction: 'DESC' },
                { expression: { type: 'fn', fn: 'random' } }
            ]
        });

        expect(newQuery.order).to.eql({
            type: 'orderby',
            orders: [
                {
                    type: 'order',
                    expr: { type: 'ident', val: 'name', quote: true },
                    dir: 'ASC'
                },
                {
                    type: 'order',
                    expr: { type: 'path', path: ['order', 'id'], quote: true },
                    dir: 'DESC'
                },
                {
                    type: 'order',
                    expr: { type: 'fn', fn: 'random' },
                    dir: 'ASC'
                }
            ]
        });
    });

    it('should throw an error if an invalid column supplied', () => {
        expect(() => xform(schema, {
            order: { column: { type: 'bogus' } }
        })).to.throw();
    });
});