import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/addFrom';

describe('Adding from clause', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('Should add a from for the query target', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                ]
            },
            filter: null
        });

        expect(newQuery.from).to.eql({
            type: 'from',
            tables: [
                {
                    type: 'path',
                    path: [ 'order' ],
                    quote: true
                }
            ]
        });
    });
});