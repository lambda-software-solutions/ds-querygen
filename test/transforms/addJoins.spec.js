import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/addJoins';

describe('Adding joins', () => {
    let schema;

    beforeEach(() => {
        schema = Schema.from(testSchema);
    });

    it('should not join any collections when only the target is involved', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                ]
            }
        });

        expect(newQuery.joins).to.be.undefined;
    });

    it('should add a join for a collection higher than the target', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'service_item',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['order', 'id'] },
                ]
            }
        });

        expect(newQuery.joins).not.to.be.undefined;
        expect(newQuery.joins.type).to.equal('joins');
        expect(newQuery.joins.joins.length).to.equal(1);

        const join = newQuery.joins.joins[0];
        expect(join.type).to.equal('join');
        expect(join.table.val).to.equal('order');
        expect(join.joinType).to.equal('INNER');
        expect(join.expr).to.eql({
            type: 'eq',
            lh: {
                type: 'path',
                quote: true,
                path: ['service_item', 'order_id']
            },
            rh: {
                type: 'path',
                quote: true,
                path: ['order', 'id']
            }
        });
    });

    it('should join multiple columns up from the target', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'service_item',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['user', 'id'] },
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);

        const join0 = newQuery.joins.joins[0];
        const join1 = newQuery.joins.joins[1];

        expect(join0.table.val).to.equal('order');
        expect(join1.table.val).to.equal('user');
    });

    it('should add a join for a collection lower than the target', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'id'] },
                    {
                        type: 'count',
                        expr: { type: 'path', path: ['service_item', 'id'] },
                    }
                ]
            }
        });

        expect(newQuery.joins).not.to.be.undefined;
        expect(newQuery.joins.type).to.equal('joins');
        expect(newQuery.joins.joins.length).to.equal(1);

        const join = newQuery.joins.joins[0];
        expect(join.type).to.equal('join');
        expect(join.table.val).to.equal('service_item');
        expect(join.joinType).to.equal('LEFT OUTER');
        expect(join.expr).to.eql({
            type: 'eq',
            lh: {
                type: 'path',
                quote: true,
                path: ['order', 'id']
            },
            rh: {
                type: 'path',
                quote: true,
                path: ['service_item', 'order_id']
            }
        });
    });

    it('should join multiple columns down from the target', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'user',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['order', 'id'] },
                    { type: 'path', path: ['user', 'id'] },
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);

        const join0 = newQuery.joins.joins[0];
        const join1 = newQuery.joins.joins[1];

        expect(join0.table.val).to.equal('order');
        expect(join1.table.val).to.equal('service_item');
    });

    it('should transitively join multiple collections between the target and an ancestor', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'service_item',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['user', 'id'] },
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);

        const join0 = newQuery.joins.joins[0];
        const join1 = newQuery.joins.joins[1];

        expect(join0.table.val).to.equal('order');
        expect(join1.table.val).to.equal('user');
    });

    it('should transitively join multiple collections between the target and a descendant', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'user',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['user', 'id'] },
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);

        const join0 = newQuery.joins.joins[0];
        const join1 = newQuery.joins.joins[1];

        expect(join0.table.val).to.equal('order');
        expect(join1.table.val).to.equal('service_item');
    });

    it('should use an extended path to disambiguate multiple paths to descendant', () => {
        const viaOrder = xform(schema, {
            type: 'query',
            _target: 'user',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['order', 'transaction', 'id'] }
                ]
            }
        });

        expect(viaOrder.joins.joins.length).to.equal(2);
        expect(viaOrder.joins.joins[0].table.val).to.equal('order');
        expect(viaOrder.joins.joins[1].table.val).to.equal('transaction');
        expect(viaOrder.joins.joins[1].expr.lh.path).to.eql(['order', 'transaction_id']);
        expect(viaOrder.joins.joins[1].expr.rh.path).to.eql(['transaction', 'id']);


        const viaServiceItem = xform(schema, {
            type: 'query',
            _target: 'user',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'transaction', 'id'] }
                ]
            }
        });

        expect(viaServiceItem.joins.joins.length).to.equal(3);
        expect(viaServiceItem.joins.joins[0].table.val).to.equal('order');
        expect(viaServiceItem.joins.joins[1].table.val).to.equal('service_item');
        expect(viaServiceItem.joins.joins[2].table.val).to.equal('transaction');
    });

    it('should pick the shortest possible path on an ambiguous join', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'user',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['transaction', 'id'] }
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);
        expect(newQuery.joins.joins[0].table.val).to.equal('order');
        expect(newQuery.joins.joins[1].table.val).to.equal('transaction');
    });

    it('should join 2 sibling collections on a single parent', () => {
        const newQuery = xform(schema, {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['service_item', 'id'] },
                    { type: 'path', path: ['location', 'id'] },
                ]
            }
        });

        expect(newQuery.joins.joins.length).to.equal(2);
        expect(newQuery.joins.joins.map(j => j.table.val)).to.include.members(['service_item', 'location']);
    });

    describe('subqueries', () => {

        it('should not add joins to a subquery', () => {
            const newQuery = xform(schema, {
                type: 'query',
                _target: 'order',
                select: {
                    type: 'select',
                    exprs: [
                        {
                            type: 'subquery',
                            select: {
                                type: 'select',
                                exprs: [
                                    { type: 'path', path: ['order', 'id'] }
                                ]
                            }
                        },
                    ]
                }
            });

            expect(newQuery.select.exprs[0].joins).to.be.undefined;
        });
    });
});