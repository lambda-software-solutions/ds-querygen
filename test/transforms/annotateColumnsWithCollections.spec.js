import { expect } from 'chai';
import testSchema from '../testSchema.yaml';
import Schema from '../../src/Schema';
import xform from '../../src/transforms/annotateColumnsWithCollections';

describe('Annotating columns with collections', () => {
    let schema;
    let query;

    beforeEach(() => {
        schema = Schema.from(testSchema);
        query = {
            type: 'query',
            _target: 'order',
            select: {
                type: 'select',
                exprs: [
                    { type: 'ident', val: 'id' },
                    { type: 'ident', val: 'id', quote: true },
                ]
            },
            where: {
                type: 'where',
                expr: {
                    type: 'pred',
                    op: 'and',
                    args: [
                        {
                            type: 'pred',
                            op: 'contains',
                            lh: { type: 'ident', val: 'notes' },
                            rh: 'test'
                        },
                        {
                            type: 'pred',
                            op: 'eq',
                            lh: { type: 'path', path: ['service_item', 'order_id'] },
                            rh: { type: 'path', path: ['id'], quote: true }
                        }
                    ]
                }
            }
        };
    });

    it('Should add the target collection to bare columns', () => {
        const newQuery = xform(schema, query);
        expect(newQuery.select.exprs).to.eql([
            { type: 'path', path: ['order', 'id'], quote: false, _collection: 'order' },
            { type: 'path', path: ['order', 'id'], quote: true, _collection: 'order' }
        ]);

        expect(newQuery.where.expr.args[0].lh).to.eql(
            { type: 'path', path: ['order', 'notes'], quote: false, _collection: 'order' },
        );
    });

    it('Should prepend the target collection to single-segment path columns', () => {
        query.select.exprs = [
            { type: 'path', path: ['id'], quote: false }
        ];

        const newQuery = xform(schema, query);
        expect(newQuery.select.exprs).to.eql([
            { type: 'path', path: ['order', 'id'], quote: false, _collection: 'order' }
        ]);

        expect(newQuery.where.expr.args[1].rh).to.eql(
            { type: 'path', path: ['order', 'id'], quote: true, _collection: 'order' },
        );
    });

    it('Should not only annotate qualified columns', () => {
        query.select.exprs = [
            { type: 'path', path: ['order', 'id'], quote: false },
            { type: 'path', path: ['service_item', 'id'], quote: true }
        ];

        const newQuery = xform(schema, query);
        expect(newQuery.select.exprs).to.eql([
            { type: 'path', path: ['order', 'id'], quote: false, _collection: 'order' },
            { type: 'path', path: ['service_item', 'id'], quote: true, _collection: 'service_item' }
        ]);

        expect(newQuery.where.expr.args[1].lh).to.eql(
            { type: 'path', path: ['service_item', 'order_id'], _collection: 'service_item' }
        );
    });
});