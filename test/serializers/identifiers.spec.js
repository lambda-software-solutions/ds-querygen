const { expect } = require('chai');
const registerIdentifiers = require('../../src/serializers/identifiers');
const Serializer = require('../../src/Serializer');

describe('identifier serialization', () => {
    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
        registerIdentifiers(serializer);
    });

    it('should serialize an identifier', () => {
        const node = { type: 'ident', val: 'my_ident' };
        expect(serializer.serialize(node)).to.equal('my_ident');
    });

    it('should serialize a quoted identifier', () => {
        const node = { type: 'ident', val: 'my_ident', quote: true };
        expect(serializer.serialize(node)).to.equal('"my_ident"');
    });

    it('should escape a double quote within a quoted identifier', () => {
        const node = { type: 'ident', val: 'my "ident"', quote: true };
        expect(serializer.serialize(node)).to.equal('"my ""ident"""');
    });

    it('should serialize a path', () => {
        const node = { type: 'path', path: ['a', 'b'] };
        expect(serializer.serialize(node)).to.equal('a.b');
    });

    it('should serialize a quoted path', () => {
        const node = { type: 'path', path: ['a', 'b'], quote: true };
        expect(serializer.serialize(node)).to.equal('"a"."b"');
    });

    it('should not quote a wildcard', () => {
        const node = { type: 'path', path: ['a', '*'], quote: true };
        expect(serializer.serialize(node)).to.equal('"a".*');
    });

    describe('JSON property', () => {

        it('should serialize a single-segment JSON property', () => {
            const node = { type: 'jsonprop', path: ['a', 'b'], property: [ 'foo' ] };
            expect(serializer.serialize(node)).to.equal(`a.b ->> 'foo'`);
        });

        it('should serialize a quoted path to a JSON property', () => {
            const node = { type: 'jsonprop', path: ['a', 'b'], property: [ 'foo' ], quote: true };
            expect(serializer.serialize(node)).to.equal(`"a"."b" ->> 'foo'`);
        });

        it('should serialize a multi-segment JSON property', () => {
            const node = { type: 'jsonprop', path: ['a', 'b'], property: [ 'foo', 'bar' ] };
            expect(serializer.serialize(node)).to.equal(`a.b -> 'foo' ->> 'bar'`);
        });

        it('should serialize a typed json property as a CASTed lookup', () => {
            const node = { type: 'jsonprop', path: ['a', 'b'], property: [ 'foo', 'n' ], datatype: 'integer' };
            expect(serializer.serialize(node)).to.equal(`CAST(a.b -> 'foo' ->> 'n' AS integer)`);
        });
    });
});