const { expect } = require('chai');
const registerPrimitives = require('../../src/serializers/primitives');
const registerIdentifiers = require('../../src/serializers/identifiers');
const registerQueries = require('../../src/serializers/queries');
const registerClauses = require('../../src/serializers/clauses');
const registerExpressions = require('../../src/serializers/expressions');
const Serializer = require('../../src/Serializer');

describe('expression serialization', () => {

    let serializer;
    beforeEach(() => {
        serializer = new Serializer();
        registerPrimitives(serializer);
        registerIdentifiers(serializer);
        registerQueries(serializer);
        registerClauses(serializer);
        registerExpressions(serializer);
    });

    describe('comparison operations', () => {
        [
            { type: 'eq', operator: '=' },
            { type: 'neq', operator: '<>' },
            { type: 'gt', operator: '>' },
            { type: 'gte', operator: '>=' },
            { type: 'lt', operator: '<' },
            { type: 'lte', operator: '<=' },
        ].forEach(({ type, operator }) => {
            it(`Should serialize ${type} as ${operator}`, () => {
                const node = {
                    type,
                    lh: 5,
                    rh: 6
                };
                expect(serializer.serialize(node)).to.equal(`5 ${operator} 6`);
            });
        });

        it(`Should serialize between`, () => {
            const node = {
                type: 'between',
                lh: 23,
                min: 17,
                max: 42
            };
            expect(serializer.serialize(node)).to.equal('23 BETWEEN 17 AND 42');
        });
    });

    describe('logical operations', () => {

        it('should serialize "not"', () => {
            const node = { type: 'not', val: true };
            expect(serializer.serialize(node)).to.equal('NOT TRUE');
        });

        it('should serialize "and"', () => {
            const node = { type: 'and', args: [ true, false, true ] };
            expect(serializer.serialize(node)).to.equal('(TRUE AND FALSE AND TRUE)');
        });

        it('should serialize "or"', () => {
            const node = { type: 'or', args: [ true, false, true ] };
            expect(serializer.serialize(node)).to.equal('(TRUE OR FALSE OR TRUE)');
        });
    });

    describe('mathematical operators', () => {

        it('should serialize addition', () => {
            const node = { type: 'add', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 + 7');
        });

        it('should serialize subtraction', () => {
            const node = { type: 'sub', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 - 7');
        });

        it('should serialize multiplication', () => {
            const node = { type: 'mlt', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 * 7');
        });

        it('should serialize division', () => {
            const node = { type: 'div', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 / 7');
        });

        it('should serialize modulo', () => {
            const node = { type: 'mod', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 % 7');
        });

        it('should serialize exponentiation', () => {
            const node = { type: 'exp', lh: 42, rh: 7 };
            expect(serializer.serialize(node)).to.equal('42 ^ 7');
        });

        it('should serialize square root', () => {
            const node = { type: 'sqrt', val: 42 };
            expect(serializer.serialize(node)).to.equal('|/ 42');
        });

        it('should serialize absolute value', () => {
            const node = { type: 'abs', val: 42 };
            expect(serializer.serialize(node)).to.equal('@ 42');
        });
    });

    describe('string operations', () => {

        it('should serialze concat', () => {
            const node = { type: 'concat', args: [ 'a', 'b', 'c' ] };
            expect(serializer.serialize(node)).to.equal('(\'a\' || \'b\' || \'c\')');
        });

        it('should serialize string length', () => {
            const node = { type: 'strlen', val: 'a' };
            expect(serializer.serialize(node)).to.equal('char_length(\'a\')');
        });

        it('should serialize lowercase', () => {
            const node = { type: 'lower', val: 'a' };
            expect(serializer.serialize(node)).to.equal('lower(\'a\')');
        });

        it('should serialize uppercase', () => {
            const node = { type: 'upper', val: 'a' };
            expect(serializer.serialize(node)).to.equal('upper(\'a\')');
        });

        it('should serialize substring', () => {
            const nodeStart = { type: 'substr', val: 'aaa', start: 1, len: 2 };
            const nodeNoStart = { type: 'substr', val: 'aaa', len: 1 };
            expect(serializer.serialize(nodeStart)).to.equal('substring(\'aaa\' from 1 for 2)');
            expect(serializer.serialize(nodeNoStart)).to.equal('substring(\'aaa\' from 1 for 1)');
        });

        it('should serialize like', () => {
            const nodeCaseSensitive = { type: 'like', lh: 'a', rh: 'a%' };
            const nodeCaseInsensitive = { type: 'like', lh: 'a', rh: 'a%', i: true };

            expect(serializer.serialize(nodeCaseSensitive)).to.equal('\'a\' LIKE \'a%\'');
            expect(serializer.serialize(nodeCaseInsensitive)).to.equal('\'a\' ILIKE \'a%\'');
        });

        it('should serialize RegExp match', () => {
            const nodeCaseSensitive = { type: 'match', lh: 'a', rh: 'a.*' };
            const nodeCaseInsensitive = { type: 'match', lh: 'a', rh: 'a.*', i: true };

            expect(serializer.serialize(nodeCaseSensitive)).to.equal('\'a\' ~ \'a.*\'');
            expect(serializer.serialize(nodeCaseInsensitive)).to.equal('\'a\' ~* \'a.*\'');
        });

        it('should serialize negated RegExp match', () => {
            const nodeCaseSensitive = { type: 'nmatch', lh: 'a', rh: 'a.*' };
            const nodeCaseInsensitive = { type: 'nmatch', lh: 'a', rh: 'a.*', i: true };

            expect(serializer.serialize(nodeCaseSensitive)).to.equal('\'a\' !~ \'a.*\'');
            expect(serializer.serialize(nodeCaseInsensitive)).to.equal('\'a\' !~* \'a.*\'');
        });
    });

    describe('list expressions', () => {

        it('should serialize in list', () => {
            const node = { type: 'inlist', lh: 1, rh: [1, 2, 3] };
            expect(serializer.serialize(node)).to.equal('1 IN (1, 2, 3)');
        });

        it('should serialize not in list', () => {
            const node = { type: 'ninlist', lh: 1, rh: [1, 2, 3] };
            expect(serializer.serialize(node)).to.equal('1 NOT IN (1, 2, 3)');
        });
    });

    describe('date and time functions', () => {

        it('should serialize current_date', () => {
            const node = { type: 'current_date' };
            expect(serializer.serialize(node)).to.equal("CURRENT_DATE");
        });

        it('should serialize extract (timestamp)', () => {
            const node = { type: 'extract', field: 'date', timestamp: '2018-10-10T16:00:00.000Z' };
            expect(serializer.serialize(node)).to.equal("EXTRACT(DATE FROM TIMESTAMP '2018-10-10T16:00:00.000Z')");
        });

        it('should serialize extract (expression)', () => {
            const node = { type: 'extract', field: 'epoch', timestamp: { type: 'ident', val: 'created_at' } };
            expect(serializer.serialize(node)).to.equal("EXTRACT(EPOCH FROM created_at)");
        });

        it('should serialize extract (interval)', () => {
            const node = { type: 'extract', field: 'date', interval: '1 month' };
            expect(serializer.serialize(node)).to.equal("EXTRACT(DATE FROM INTERVAL '1 month')");
        });
    });

    describe('type-related functions', () => {

        it('should cast an expression as a data type', () => {
            const node = { type: 'cast', expr: '1 day', datatype: 'interval' };
            expect(serializer.serialize(node)).to.equal("CAST('1 day' AS INTERVAL)");
        });
    });

    describe('null checks', () => {

        it('should serialize null', () => {
            const node = { type: 'null' };
            expect(serializer.serialize(node)).to.equal("NULL");
        });

        it('should serialize is null', () => {
            const node = { type: 'isNull', expr: 42 };
            expect(serializer.serialize(node)).to.equal("42 IS NULL");
        });

        it('should serialize is not null', () => {
            const node = { type: 'isNotNull', expr: 42 };
            expect(serializer.serialize(node)).to.equal("42 IS NOT NULL");
        });
    });

    describe('subquery expressions', () => {

        it('should serialize exists', () => {
            const node = {
                type: 'exists',
                subquery: {
                    type: 'subquery',
                    select: {
                        type: 'select',
                        exprs: [ 42 ]
                    }
                }
            };
            expect(serializer.serialize(node)).to.equal("EXISTS (SELECT 42)");
        });
    });
});