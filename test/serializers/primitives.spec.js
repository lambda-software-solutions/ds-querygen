const { expect } = require('chai');
const registerPrimitives = require('../../src/serializers/primitives');
const Serializer = require('../../src/Serializer');

describe('primitive serialization', () => {

    let serializer;
    beforeEach(() => {
        serializer = new Serializer();
        registerPrimitives(serializer);
    });

    it('Should serialze a number', () => {
        expect(serializer.serialize(42)).to.equal('42');
        expect(serializer.serialize(3.14159)).to.equal('3.14159');
    });

    it('Should serialze a boolean', () => {
        expect(serializer.serialize(true)).to.equal('TRUE');
        expect(serializer.serialize(false)).to.equal('FALSE');
    });

    it('Should serialze an array', () => {
        expect(serializer.serialize([1,2,3])).to.equal('(1, 2, 3)')
    });

    it('should serialze a timestamp', () => {
        expect(serializer.serialize({
            type: 'timestamp',
            timestamp: '2018-10-10T16:00:00.000Z'
        })).to.equal("TIMESTAMP '2018-10-10T16:00:00.000Z'");
    });

    it('should serialze a date', () => {
        expect(serializer.serialize({
            type: 'date',
            date: '2018-10-10'
        })).to.equal("DATE '2018-10-10'");
    });

    it('should serialze an interval', () => {
        expect(serializer.serialize({
            type: 'interval',
            interval: 'day',
            span: 5
        })).to.equal("INTERVAL '5 DAY'");
    });

    describe('strings', () => {
        it('Should serialize a basic string', () => {
            expect(serializer.serialize('Not interesting')).to.equal("'Not interesting'");
        });

        it('Should serialize a string with single quotes', () => {
            expect(serializer.serialize('I\'m interesting')).to.equal("'I''m interesting'");
        });

        it('Should serialize escape sequences', () => {
            expect(serializer.serialize('a\bb')).to.equal("'a\\bb'");
            expect(serializer.serialize('a\fb')).to.equal("'a\\fb'");
            expect(serializer.serialize('a\nb')).to.equal("'a\\nb'");
            expect(serializer.serialize('a\rb')).to.equal("'a\\rb'");
            expect(serializer.serialize('a\tb')).to.equal("'a\\tb'");
            expect(serializer.serialize('a\\b')).to.equal("'a\\\\b'");
        });

        it('Should serialize uuids as strings', () => {
            expect(serializer.serialize({
                type: 'uuid',
                uuid: '494cf084-c97d-11e8-91b9-72000399adc0'
            })).to.equal("'494cf084-c97d-11e8-91b9-72000399adc0'");
        });
    });
});