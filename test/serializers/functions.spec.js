const { expect } = require('chai');
const registerPrimitives = require('../../src/serializers/primitives');
const registerIdentifiers = require('../../src/serializers/identifiers');
const registerFunctions = require('../../src/serializers/functions');
const Serializer = require('../../src/Serializer');

describe('function serialization', () => {
    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
        registerPrimitives(serializer);
        registerIdentifiers(serializer);
        registerFunctions(serializer);
    });

    it('should serialize an no-arg function', () => {
        const node = {
            type: 'fn',
            fn: 'now'
        };
        expect(serializer.serialize(node)).to.equal('NOW()');
    });

    it('should serialize an unary function', () => {
        const node = {
            type: 'fn',
            fn: 'lower',
            args: ['TeSt']
        };
        expect(serializer.serialize(node)).to.equal("LOWER('TeSt')");
    });

    it('should serialize an n-ary function', () => {
        const node = {
            type: 'fn',
            fn: 'some_fn',
            args: [1, 2, 3]
        };
        expect(serializer.serialize(node)).to.equal("SOME_FN(1,2,3)");
    });

    it('should serialize an aggregation function', () => {
        const node = {
            type: 'aggFn',
            aggFn: 'sum',
            expr: { type: 'ident', val: 'amount' }
        };
        expect(serializer.serialize(node)).to.equal('SUM(amount)');
    });
});