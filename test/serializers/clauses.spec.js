const { expect } = require('chai');
const registerIdentifiers = require('../../src/serializers/identifiers');
const registerExpressions = require('../../src/serializers/expressions');
const registerModifiers = require('../../src/serializers/modifiers');
const registerClauses = require('../../src/serializers/clauses');
const Serializer = require('../../src/Serializer');

describe('clause serialization', () => {
    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
        registerIdentifiers(serializer);
        registerExpressions(serializer);
        registerModifiers(serializer);
        registerClauses(serializer);
    });

    it('should serialize a SELECT clause', () => {
        const node = {
            type: 'select',
            exprs: [
                { type: 'ident', val: 'a' },
                {
                    type: 'alias',
                    alias: 'my_b',
                    expr: { type: 'ident', val: 'b', quote: true }
                }
            ]
        };

        expect(serializer.serialize(node)).to.equal('SELECT a, "b" AS my_b')
    });

    it('should serialize a FROM clause', () => {
        const node = {
            type: 'from',
            tables: [
                {
                    type: 'alias',
                    alias: 'worker',
                    expr: { type: 'ident', val: 'person' }
                },
                { type: 'ident', val: 'company' }
            ]
        };

        expect(serializer.serialize(node)).to.equal('FROM person AS worker, company')
    });

    it('should serialize a natural join', () => {
        const node = {
            type: 'join',
            table: {
                type: 'alias',
                alias: 'p',
                expr: { type: 'ident', val: 'person' }
            },
            expr: {
                type: 'eq',
                lh: { type: 'path', path: ['company', 'id'] },
                rh: { type: 'path', path: ['p', 'company_id'] },
            }
        };

        expect(serializer.serialize(node)).to.equal('JOIN person AS p ON company.id = p.company_id')
    });

    it('should serialize a specified joins', () => {
        const node = {
            type: 'joins',
            joins: [
                {
                    type: 'join',
                    joinType: 'left outer',
                    table: {
                        type: 'alias',
                        alias: 'p',
                        expr: { type: 'ident', val: 'person' }
                    },
                    expr: {
                        type: 'eq',
                        lh: { type: 'path', path: ['company', 'id'] },
                        rh: { type: 'path', path: ['p', 'company_id'] },
                    }
                }
            ]
        };

        expect(serializer.serialize(node)).to.equal('LEFT OUTER JOIN person AS p ON company.id = p.company_id')
    });

    it('should serialze a grouping', () => {
        const node = {
            type: 'groupby',
            dimensions: [
                { type: 'ident', val: 'name' },
                { type: 'ident', val: 'age' }
            ]
        };

        expect(serializer.serialize(node)).to.equal('GROUP BY name, age');
    });

    it('should serialize an ordering', () => {
        const node = {
            type: 'orderby',
            orders: [
                {
                    type: 'order',
                    expr: { type: 'ident', val: 'age' },
                    dir: 'desc'
                },
                {
                    type: 'order',
                    expr: { type: 'ident', val: 'name' },
                    dir: 'asc'
                },
            ]
        };

        expect(serializer.serialize(node)).to.equal('ORDER BY age DESC, name ASC');
    });

    it('should serialize a limit', () => {
        const node = {
            type: 'limit',
            limit: 100
        };

        expect(serializer.serialize(node)).to.equal('LIMIT 100');
    });

    it('should serialize a limit with an offset', () => {
        const node = {
            type: 'limit',
            limit: 100,
            offset: 5280
        };

        expect(serializer.serialize(node)).to.equal('LIMIT 100 OFFSET 5280');
    });
});