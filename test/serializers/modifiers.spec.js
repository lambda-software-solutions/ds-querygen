const { expect } = require('chai');
const registerIdentifiers = require('../../src/serializers/identifiers');
const registerModifiers = require('../../src/serializers/modifiers');
const Serializer = require('../../src/Serializer');

describe('modifier serialization', () => {
    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
        registerIdentifiers(serializer);
        registerModifiers(serializer);
    });

    it ('should serialize an aliased expression', () => {
        const node = {
            type: 'alias',
            alias: 'my_xyzzy',
            expr: { type: 'ident', val: 'xyzzy' }
        };
        expect(serializer.serialize(node)).to.equal('xyzzy AS my_xyzzy')
    });
});