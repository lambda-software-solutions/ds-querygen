const { expect } = require('chai');
const registerPrimitives = require('../../src/serializers/primitives');
const registerIdentifiers = require('../../src/serializers/identifiers');
const registerModifiers = require('../../src/serializers/modifiers');
const registerExpressions = require('../../src/serializers/expressions');
const registerClauses = require('../../src/serializers/clauses');
const registerQueries = require('../../src/serializers/queries');
const Serializer = require('../../src/Serializer');

describe('query serialization', () => {
    let serializer;

    beforeEach(() => {
        serializer = new Serializer();
        registerPrimitives(serializer);
        registerIdentifiers(serializer);
        registerModifiers(serializer);
        registerExpressions(serializer);
        registerClauses(serializer);
        registerQueries(serializer);
    });

    it('should serialize a select-only query', () => {
        const node = {
            type: 'query',
            select: {
                type: 'select',
                exprs: [ 42 ]
            }
        };

        expect(serializer.serialize(node)).to.equal('SELECT 42');
    });

    it('should serialize a subquery', () => {
        const node = {
            type: 'subquery',
            select: {
                type: 'select',
                exprs: [ 42 ]
            }
        };

        expect(serializer.serialize(node)).to.equal('(SELECT 42)');
    });

    it('should serialize a complex query', () => {
        const node = {
            type: 'query',
            select: {
                type: 'select',
                exprs: [
                    { type: 'path', path: ['worker', 'name'] },
                    {
                        type: 'alias',
                        alias: 'company_name',
                        expr: { type: 'path', path: ['c', 'name'] },
                    }
                ]
            },
            from: {
                type: 'from',
                tables: [
                    {
                        type: 'alias',
                        alias: 'worker',
                        expr: { type: 'ident', val: 'person' }
                    }
                ]
            },
            joins: {
                type: 'joins',
                joins: [
                    {
                        type: 'join',
                        joinType: 'inner',
                        table: {
                            type: 'alias',
                            alias: 'c',
                            expr: { type: 'ident', val: 'company' }
                        },
                        expr: {
                            type: 'eq',
                            lh: { type: 'path', path: ['worker', 'company_id'] },
                            rh: { type: 'path', path: ['c', 'id'] },
                        }
                    }
                ]
            },
            where: {
                type: 'where',
                expr: {
                    type: 'and',
                    args: [
                        {
                            type: 'gt',
                            lh: { type: 'path', path: ['worker', 'age'] },
                            rh: 21
                        },
                        {
                            type: 'ninlist',
                            lh: { type: 'path', path: ['c', 'country_code'] },
                            rh: [ 'BG', 'LV' ]
                        }
                    ]
                }
            },
            order: {
                type: 'orderby',
                orders: [
                    {
                        type: 'order',
                        expr: { type: 'path', path: ['worker', 'age'] },
                        dir: 'asc'
                    }
                ]
            },
            limit: {
                type: 'limit',
                limit: 50
            }
        };

        expect(serializer.serialize(node)).to.equal('SELECT worker.name, c.name AS company_name FROM person AS worker INNER JOIN company AS c ON worker.company_id = c.id WHERE (worker.age > 21 AND c.country_code NOT IN (\'BG\', \'LV\')) ORDER BY worker.age ASC LIMIT 50');
    });
});